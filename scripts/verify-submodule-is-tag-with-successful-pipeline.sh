#!/bin/bash

set -o errexit # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)
# set -o xtrace # AKA -x - get bash "stacktraces" and see where this script failed
#
# this only works when the submodule is up to date, when there are changes, the first character of the output is a `+`
SUBMODULE_SHA="$(git submodule | awk '{print $1}')"

TAG_PIPELINES_FOR_SHA=$(curl "https://gitlab.com/api/v4/projects/gitlab-org%2Fgitlab-vscode-extension/pipelines?scope=tags&sha=${SUBMODULE_SHA}")


echo "Tag pipelines for the submodule SHA: ${TAG_PIPELINES_FOR_SHA}"

STATUS_OF_LAST_TAG_PIPELINE=$(echo $TAG_PIPELINES_FOR_SHA | jq -r '.[0].status')

if [ $STATUS_OF_LAST_TAG_PIPELINE == "success" ]; then
  echo "found successful tag pipeline for the submodule sha 🎉"
else
  echo "The submodule SHA (${SUBMODULE_SHA}) doesn't have a successful tag pipeline associated with it."
  echo "Ensure that the SHA is tagged and that it has a successful pipeline in the gitlab-vscode-extesnion project."
  return 1
fi
