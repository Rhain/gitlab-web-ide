#!/usr/bin/env bash
set -e

CURRENT_VERSION=$(cat VERSION)
GITLAB_WEB_IDE_VERSION="${1}"

if [ -z "${GITLAB_WEB_IDE_VERSION}" ]
then
  GITLAB_WEB_IDE_VERSION="$(cat VERSION)-dev-$(date '+%Y%m%d%H%M%S')"
  echo "No verison was provided as argument. Using development version ${GITLAB_WEB_IDE_VERSION}..."
else
  if [[ $GITLAB_WEB_IDE_VERSION != "${CURRENT_VERSION}"* ]]
  then
    echo "Error: Version provided (${GITLAB_WEB_IDE_VERSION}) must start with the current version (${CURRENT_VERSION})."
    exit 1
  else
    echo "Using version ${GITLAB_WEB_IDE_VERSION}..."
  fi
fi

echo "Build: Building @gitlab/web-ide package..."
yarn run build:webide

echo "Pre-pack: Setting @gitlab/web-ide version to ${GITLAB_WEB_IDE_VERSION}..."
yarn workspace @gitlab/web-ide version ${GITLAB_WEB_IDE_VERSION}

# why backup package.json: We need to clean up a @gitlab/web-ide-types which is only used for compiling.
# We are not publishing @gitlab/web-ide-types package, so releasing references to this
# could cause problems...
# For this reason, let's copy off the existing package.json so we can remove this dependency
# before packing.
echo "Pre-pack: Saving backup package.json..."
cp packages/web-ide/package.json{,.bak}

# region: Clean and optimize package -----------------------------------

echo "Pre-pack (clean): Cleaning package.json for publish..."
BUNDLED_PACKAGES=$(node scripts/echo-workspace-dependencies.js packages/web-ide/package.json)
yarn workspace @gitlab/web-ide remove ${BUNDLED_PACKAGES}

# why: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/95169#note_1064410338
echo "Pre-pack (clean): Remove .map files to improve artifact size"
find packages/web-ide/dist/public -name '*.js.map' -delete

# region: Pack ---------------------------------------------------------

DEST="tmp/packages/gitlab-web-ide-${GITLAB_WEB_IDE_VERSION}.tgz"
echo "Pack: Packing @gitlab/web-ide package..."
mkdir -p tmp/packages
yarn workspace @gitlab/web-ide pack --out ${PWD}/${DEST}

# region: Teardown -----------------------------------------------------

echo "Teardown: Restoring old package.json"
rm packages/web-ide/package.json
mv packages/web-ide/package.json{.bak,}
# why --no-immutable: This will be mutable since we're resurrecting a removed workspace dependency
yarn --no-immutable

# region: Verify -------------------------------------------------------
# TODO: Read package.json and verify no workspace dependencies
