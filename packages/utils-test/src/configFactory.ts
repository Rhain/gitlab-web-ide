import type { BaseConfig, ClientOnlyConfig, RemoteConfig, AnyConfig } from '@gitlab/web-ide-types';

export const createBaseConfig = (): BaseConfig => ({
  baseUrl: 'https://foo.bar',
  handleError: jest.fn(),
  handleClose: jest.fn(),
  handleStartRemote: jest.fn(),
  handleTracking: jest.fn(),
  links: {
    feedbackIssue: 'foobar',
    userPreferences: 'user/preferences',
    signIn: 'user/signIn',
  },
});

export const createClientOnlyConfig = (): ClientOnlyConfig => ({
  ...createBaseConfig(),
  gitlabUrl: 'https://gitlab.com',
  auth: {
    type: 'token',
    token: 'very-secret-token',
  },
  projectPath: 'gitlab-org/gitlab',
  ref: 'main',
});

export const createRemoteHostConfig = (): RemoteConfig => ({
  ...createBaseConfig(),
  remoteAuthority: 'https://foo.bar.com',
  hostPath: '/',
  connectionToken: 'foobar',
});

export const createFullConfig = (): AnyConfig => ({
  ...createBaseConfig(),
  ...createClientOnlyConfig(),
  ...createRemoteHostConfig(),
});
