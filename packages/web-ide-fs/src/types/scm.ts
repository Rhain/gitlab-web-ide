import type { IFileStats } from './fs';

export enum FileStatusType {
  Modified = 1,
  Created = 2,
  Deleted = 3,
}

export interface IFileStatusModified {
  readonly type: FileStatusType.Modified;
  readonly path: string;
  readonly content: Buffer;
}

export interface IFileStatusCreated {
  readonly type: FileStatusType.Created;
  readonly path: string;
  readonly content: Buffer;
}

export interface IFileStatusDeleted {
  readonly type: FileStatusType.Deleted;
  readonly path: string;
}

export type IFileStatus = IFileStatusModified | IFileStatusCreated | IFileStatusDeleted;

/**
 * Responsible for viewing the Source Control state for the Web IDE's File System
 */
export interface ISourceControlSystem {
  status(): Promise<IFileStatus[]>;
}

export interface ISourceControlFileSystem {
  stat(path: string): Promise<IFileStats>;

  statOriginal(path: string): Promise<IFileStats>;

  readFile(path: string): Promise<Uint8Array>;

  readFileOriginal(path: string): Promise<Uint8Array>;
}
