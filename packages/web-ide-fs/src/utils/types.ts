import type { FileType, IFileStats } from '../types';

export enum BlobContentType {
  Unloaded = 1,
  Raw = 2,
}

export interface IUnloadedContent {
  type: BlobContentType.Unloaded;
  path: string;
}

export interface IRawContent {
  type: BlobContentType.Raw;
  raw: Uint8Array;
}

export interface IFileEntryBase extends IFileStats {
  readonly name: string;
}

export interface IMutableFileEntryBase extends IFileEntryBase {
  mode: number;
  ctime: number;
  mtime: number;
  size: number;
}

export interface IBlobEntry extends IFileEntryBase {
  readonly type: FileType.Blob;
  readonly content: IUnloadedContent | IRawContent;
}

export interface IMutableBlobEntry extends IMutableFileEntryBase {
  readonly type: FileType.Blob;
  content: IUnloadedContent | IRawContent;
}

export interface ITreeEntry extends IFileEntryBase {
  readonly type: FileType.Tree;
  readonly children: string[];
}

export interface IMutableTreeEntry extends IMutableFileEntryBase {
  readonly type: FileType.Tree;
  children: string[];
}

export type IFileEntry = IBlobEntry | ITreeEntry;

export type IMutableFileEntry = IMutableBlobEntry | IMutableTreeEntry;
