import type { IFileList, IFileSystem } from './types';

export class FileListWithCache implements IFileList {
  readonly #fileList: IFileList;

  readonly #fs: IFileSystem;

  #cache: string[];

  #cacheKey: number | undefined;

  constructor(fileList: IFileList, fs: IFileSystem) {
    this.#fileList = fileList;
    this.#fs = fs;

    this.#cache = [];
    // why: initialize as undefined so that we start with an invalidated cache
    this.#cacheKey = undefined;
  }

  async listAllBlobs(): Promise<string[]> {
    return this.#updateCache();
  }

  async #updateCache(): Promise<string[]> {
    const shouldUpdate = await this.#shouldUpdateCache();

    if (!shouldUpdate) {
      return this.#cache;
    }

    this.#cache = await this.#fileList.listAllBlobs();
    this.#cacheKey = await this.#generateCacheKey();
    return this.#cache;
  }

  async #shouldUpdateCache(): Promise<boolean> {
    const newKey = await this.#generateCacheKey();

    return this.#cacheKey !== newKey;
  }

  #generateCacheKey(): Promise<number> {
    return this.#fs.lastModifiedTime();
  }
}
