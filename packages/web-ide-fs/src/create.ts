import { createOverlayFSComponents, createOverlayFS } from './browserfs';
import { WebIdeFileSystemBFS } from './browserfs/WebIdeFileSystemBFS';
import { OverlaySourceControl } from './scm/OverlaySourceControl';
import { OverlaySourceControlFileSystem } from './scm/OverlaySourceControlFileSystem';
import type {
  IFileContentProvider,
  IGitLsTreeEntry,
  IFileSystem,
  ISourceControlSystem,
  ISourceControlFileSystem,
} from './types';

export interface ICreateSystemOptions {
  contentProvider: IFileContentProvider;
  gitLsTree: IGitLsTreeEntry[];
  // TODO: Rename any instance of repo.?root or repo.?path to repoRootPath
  repoRoot: string;
}

interface ISystems {
  fs: IFileSystem;
  sourceControl: ISourceControlSystem;
  sourceControlFs: ISourceControlFileSystem;
}

export const createSystems = async (options: ICreateSystemOptions): Promise<ISystems> => {
  const overlayFSComponents = await createOverlayFSComponents(options);
  const fs = await createOverlayFS(overlayFSComponents);

  return {
    fs: new WebIdeFileSystemBFS({
      fs,
      deletedFilesLog: overlayFSComponents.deletedFilesLog,
      repoRootPath: options.repoRoot,
    }),
    // TODO: Create sourceControl that hooks into writeable fs
    sourceControl: new OverlaySourceControl({
      ...overlayFSComponents,
      repoRootPath: options.repoRoot,
    }),
    sourceControlFs: new OverlaySourceControlFileSystem(fs, overlayFSComponents.readable),
  };
};
