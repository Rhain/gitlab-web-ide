import { joinPaths } from '@gitlab/utils-path';
import type { IReadonlyPromisifiedBrowserFS } from '../types';

export const readdirRecursive = async (
  fs: IReadonlyPromisifiedBrowserFS,
  path: string,
): Promise<string[]> => {
  const children = await fs.readdir(path);

  const childrenResults = await Promise.all(
    children.map(async child => {
      const childPath = joinPaths(path, child);
      const stat = await fs.stat(childPath, false);

      if (stat.isDirectory()) {
        return readdirRecursive(fs, childPath);
      }

      return [childPath];
    }),
  );

  return childrenResults.flatMap(x => x);
};
