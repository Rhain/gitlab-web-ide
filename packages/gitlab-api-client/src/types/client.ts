import type { GetRequest, PostRequest, GetBufferRequest } from '@gitlab/web-ide-interop';

export type EndpointMethod = GetRequest<unknown>['method'] | PostRequest<unknown>['method'];
export type PathParams = Record<string, string>;
export type DefaultPathParams = Record<string, never>;
export type DefaultBodyParams = never;
export type DefaultReturnType = unknown;

export interface AuthProvider {
  getHeaders(): Promise<Readonly<Record<string, string>>>;
}

export interface ResponseErrorBody {
  status: number;
  body?: unknown;
}

export interface GetEndpoint<TReturnType, TPathParams> {
  createRequest(params: TPathParams): GetRequest<TReturnType>;
}

export interface GetBufferEndpoint<TPathParams> {
  createRequest(params: TPathParams): GetBufferRequest;
}

export interface PostEndpoint<TReturnType, TPathParams, TBodyParams> {
  createRequest(pathParams: TPathParams, bodyParams: TBodyParams): PostRequest<TReturnType>;
}
