import type { ResponseErrorBody } from './types';

async function toResponseError(response: Response): Promise<ResponseErrorBody> {
  let responseBody: unknown = null;

  try {
    if (response.headers.get('Content-Type') === 'application/json') {
      responseBody = await response.json();
    } else if (response.headers.get('Content-Type')?.startsWith('text')) {
      responseBody = await response.text();
    }
  } catch {
    // eslint-disable-next-line no-console
    console.error('Failed to parse response body', response);
  }

  return {
    status: response.status,
    body: responseBody,
  };
}

export async function createResponseError(response: Response) {
  return new Error(JSON.stringify(await toResponseError(response)));
}
