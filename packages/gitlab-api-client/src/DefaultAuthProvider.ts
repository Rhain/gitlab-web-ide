import type { AuthProvider } from './types';

export class DefaultAuthProvider implements AuthProvider {
  #headers: Readonly<Record<string, string>>;

  constructor(headers: Readonly<Record<string, string>>) {
    this.#headers = headers;
  }

  getHeaders(): Promise<Readonly<Record<string, string>>> {
    return Promise.resolve(this.#headers);
  }
}

export const createPrivateTokenProvider = (token: string) =>
  new DefaultAuthProvider({
    'PRIVATE-TOKEN': token,
  });
