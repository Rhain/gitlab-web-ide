import { DefaultAuthProvider, createPrivateTokenProvider } from './DefaultAuthProvider';

describe('DefaultAuthProvider', () => {
  it('creates auth provider for given headers', async () => {
    const testHeaders = {
      'TEST-HEAD': 'ABCDEF',
      'TEST-2': '123456',
    };

    const provider = new DefaultAuthProvider(testHeaders);

    await expect(provider.getHeaders()).resolves.toEqual(testHeaders);
  });

  describe('createPrivateTokenProvider', () => {
    it('creates auth provider for private token', async () => {
      const testToken = 'ABCDEF';

      const provider = createPrivateTokenProvider(testToken);

      await expect(provider.getHeaders()).resolves.toEqual({
        'PRIVATE-TOKEN': testToken,
      });
    });
  });
});
