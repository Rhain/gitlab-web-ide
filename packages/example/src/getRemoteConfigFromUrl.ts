import type { ExampleConfigPayload } from './types';

// http://127.0.0.1:8000/?remoteHost=localhost:9888&hostPath=/Users/tomas/workspace/gitlab-vscode-extension
export const getRemoteConfigFromUrl = (
  locationSearch = window.location.search,
): ExampleConfigPayload | undefined => {
  const queryParams = new URLSearchParams(locationSearch);
  const remoteAuthority = queryParams.get('remoteHost') || '';
  const hostPath = queryParams.get('hostPath') || '';
  // If the URL contains `remoteHost` or `hostPath`, we assume that user wants to use WebIDE in remote development mode
  if (remoteAuthority || hostPath) {
    return {
      type: 'remote',
      config: {
        remoteAuthority,
        hostPath,
        connectionToken: '',
      },
    };
  }
  return undefined;
};
