import type { ConfigType, AuthType } from '@gitlab/web-ide-types';

export interface ExampleConfigClientOnly {
  gitlabUrl: string;
  projectPath: string;
  gitRef: string;
  codeSuggestionsEnabled: boolean;
  authType?: AuthType;
  gitlabToken: string;
  clientId: string;
  telemetryEnabled: boolean;
}

export interface ExampleConfigRemote {
  remoteAuthority: string;
  hostPath: string;
  connectionToken: string;
}

export type ExampleConfigKeys = keyof ExampleConfigClientOnly | keyof ExampleConfigRemote;

export const SENSITIVE_KEYS: ExampleConfigKeys[] = ['connectionToken', 'gitlabToken'];

/**
 * Payload object that represents the user's settings and is saved/loaded from local storage
 */
export interface ExampleConfigPayload {
  type: ConfigType;
  config: ExampleConfigClientOnly | ExampleConfigRemote;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const isExampleConfigPayload = (obj: any): obj is ExampleConfigPayload =>
  obj && typeof obj === 'object' && typeof obj.type === 'string' && typeof obj.config === 'object';
