export const getBaseUrlFromLocation = () => {
  const newUrl = new URL('web-ide/public', window.location.href);

  return newUrl.href;
};

export const getRootUrlFromLocation = () => {
  const newUrl = new URL('/', window.location.href);

  return newUrl.href;
};

export const getOAuthCallbackUrl = () => {
  const url = new URL(window.location.href);
  const newUrl = new URL('oauth_callback.html', window.location.href);

  const username = url.searchParams.get('username');

  if (username) {
    newUrl.searchParams.set('username', username);
  }

  return newUrl.href;
};
