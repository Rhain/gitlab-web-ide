import type { ConfigType, AnyConfig } from '@gitlab/web-ide-types';
import { createClientOnlyConfig, createRemoteHostConfig } from '@gitlab/utils-test';
import { getIframeHtml } from './getIframeHtml';

const clientOnlyConfig = createClientOnlyConfig();
const remoteHostConfig = createRemoteHostConfig();

describe('getIframeHtml', () => {
  const collectAttributes = (el: Element): Record<string, string> =>
    Array.from(el.attributes).reduce(
      (acc, attr) => Object.assign(acc, { [attr.name]: attr.value }),
      {},
    );

  // In a friendly school bus driver sort of way...
  // Not a Pennywise-The-Clown-From-It way
  const collectChildren = (el: Element) =>
    Array.from(el.children)
      .map(child => ({
        tag: child.tagName.toLowerCase(),
        attributes: collectAttributes(child),
        content: child.innerHTML.trim(),
      }))
      .reduce((acc: Record<string, Record<string, string>[]>, { tag, attributes, content }) => {
        acc[tag] = acc[tag] ?? [];

        if (Object.entries(attributes).length) {
          acc[tag].push(attributes);
        }

        if (content) {
          acc[tag].push({ content });
        }

        return acc;
      }, {});

  const createTestSubject = (configType: ConfigType, config: AnyConfig) => {
    const html = getIframeHtml(configType, config);

    const parser = new DOMParser();
    const document = parser.parseFromString(html, 'text/html');

    return {
      head: collectChildren(document.head),
      body: collectChildren(document.body),
    };
  };

  type TestParameters = {
    configType: ConfigType;
    config: AnyConfig;
    scriptAttrs: Record<string, string>;
  };

  it.each`
    desc                           | configType       | config                                         | scriptAttrs
    ${'client only config'}        | ${'client-only'} | ${clientOnlyConfig}                            | ${{}}
    ${'remote config'}             | ${'remote'}      | ${remoteHostConfig}                            | ${{}}
    ${'with nonce'}                | ${'client-only'} | ${{ ...clientOnlyConfig, nonce: 'noncense' }}  | ${{ nonce: 'noncense' }}
    ${'with erroneous configType'} | ${'test"123'}    | ${clientOnlyConfig}                            | ${{}}
    ${'with erroneous baseUrl'}    | ${'client-only'} | ${{ ...clientOnlyConfig, baseUrl: '.."123.' }} | ${{}}
  `(
    'with $desc, returns properly escaped head and body elements',
    ({ configType, config, scriptAttrs }: TestParameters) => {
      const subject = createTestSubject(configType, config);

      expect(subject).toStrictEqual({
        head: {
          meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: expect.any(String) },
            { id: 'gl-config-type', 'data-settings': configType },
            { id: 'gl-config-json', 'data-settings': JSON.stringify(config) },
          ],
          link: [
            {
              'data-name': 'vs/workbench/workbench.web.main',
              rel: 'stylesheet',
              href: `${config.baseUrl}/vscode/out/vs/workbench/workbench.web.main.css`,
            },
          ],
        },
        body: {
          script: [
            {
              src: `${config.baseUrl}/main.js`,
              ...scriptAttrs,
            },
          ],
        },
      });
    },
  );

  it('does not inject fonts if not configured', () => {
    const subject = createTestSubject('client-only', clientOnlyConfig);

    const preloadLinks = subject.head.link.filter(link => link.as === 'font');
    expect(preloadLinks).toHaveLength(0);
    expect(subject.head.style).toBeUndefined();
  });

  it('injects the fonts if they are configured', () => {
    const subject = createTestSubject('client-only', {
      ...clientOnlyConfig,
      editorFont: {
        fallbackFontFamily: '"Deja Vu Sans Mono", monospace',
        fontFaces: [
          {
            family: 'GitLab Mono',
            src: [
              {
                format: 'woff2',
                url: 'http://example.com/fonts/GitLabMono.woff2',
              },
            ],
          },
          {
            family: 'GitLab Mono',
            style: 'italic',
            src: [
              {
                url: 'http://example.com/fonts/GitLabMonoItalic.woff2',
                format: 'woff2',
              },
            ],
          },
          {
            family: "A tester's font",
            src: [
              {
                format: 'opentype',
                url: 'http://example.com/fonts/FooBar.otf',
              },
            ],
            display: 'optional',
            unicodeRange: 'U+0025-00FF',
          },
          {
            family: 'Font with multiple assets',
            src: [
              {
                format: 'opentype',
                url: 'http://example.com/fonts/MultiFont1.otf',
              },
              {
                format: 'woff2',
                url: 'http://example.com/fonts/MultiFont2.woff2',
              },
            ],
          },
        ],
      },
    });

    expect(subject.head.link).toEqual(
      expect.arrayContaining([
        {
          rel: 'preload',
          as: 'font',
          type: 'font/woff2',
          crossorigin: '',
          href: 'http://example.com/fonts/GitLabMono.woff2',
        },
        {
          rel: 'preload',
          as: 'font',
          type: 'font/woff2',
          crossorigin: '',
          href: 'http://example.com/fonts/GitLabMonoItalic.woff2',
        },
        {
          rel: 'preload',
          as: 'font',
          type: 'font/opentype',
          crossorigin: '',
          href: 'http://example.com/fonts/FooBar.otf',
        },
        {
          rel: 'preload',
          as: 'font',
          type: 'font/opentype',
          crossorigin: '',
          href: 'http://example.com/fonts/MultiFont1.otf',
        },
        {
          rel: 'preload',
          as: 'font',
          type: 'font/woff2',
          crossorigin: '',
          href: 'http://example.com/fonts/MultiFont2.woff2',
        },
      ]),
    );

    expect(subject.head.style[0].content).toMatchInlineSnapshot(`
      "@font-face {
      font-family: 'GitLab Mono';
      src: url('http://example.com/fonts/GitLabMono.woff2') format('woff2');
      }
      @font-face {
      font-family: 'GitLab Mono';
      font-style: italic;
      src: url('http://example.com/fonts/GitLabMonoItalic.woff2') format('woff2');
      }
      @font-face {
      font-family: 'A tester%27s font';
      font-display: optional;
      unicode-range: U+0025-00FF;
      src: url('http://example.com/fonts/FooBar.otf') format('opentype');
      }
      @font-face {
      font-family: 'Font with multiple assets';
      src: url('http://example.com/fonts/MultiFont1.otf') format('opentype'),
        url('http://example.com/fonts/MultiFont2.woff2') format('woff2');
      }"
    `);
  });
});
