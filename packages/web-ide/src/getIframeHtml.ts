import type { AnyConfig, ConfigType, WebIDEFontFace } from '@gitlab/web-ide-types';
import { escapeCssQuotedValue, escapeHtml } from '@gitlab/utils-escape';

import { kebabCase } from './kebabCase';

const getFontLinkHeader = (fontFace: WebIDEFontFace) =>
  fontFace.src
    .map(
      src => `<link
  rel="preload"
  as="font"
  type="font/${escapeHtml(src.format)}"
  crossorigin
  href="${escapeHtml(src.url)}"
/>`,
    )
    .join('\n');

const getFontFaceDefinition = (fontFace: WebIDEFontFace) => {
  const keys = Object.keys(fontFace) as Array<keyof typeof fontFace>;

  const fontFaceBody: string[] = keys
    .filter(key => fontFace[key] && key !== 'src')
    .map(key => {
      switch (key) {
        case 'display':
        case 'family':
        case 'featureSettings':
        case 'stretch':
        case 'style':
        case 'variationSettings':
        case 'weight': {
          const value =
            key === 'family' ? `'${escapeCssQuotedValue(fontFace[key])}'` : fontFace[key];
          // These properties of FontFace don't correspond exactly to @font-face
          // descriptors, so rename them accordingly.
          return `font-${kebabCase(key)}: ${value};`;
        }
        default:
          // Assume remaining properties don't need renaming beyond kebab-casing,
          // e.g., ascent-override.
          return `${kebabCase(key)}: ${fontFace[key]};`;
      }
    });

  const srcDeclarations = fontFace.src
    .map((src, idx) => {
      const url = escapeCssQuotedValue(src.url);
      const format = escapeCssQuotedValue(src.format);
      const prefix = idx > 0 ? '  ' : '';

      return `${prefix}url('${url}') format('${format}')`;
    })
    .join(',\n');

  fontFaceBody.push(`src: ${srcDeclarations};`);

  return `@font-face {\n${fontFaceBody.join('\n')}\n}`;
};

const getFontHeader = (fontFaces: WebIDEFontFace[] | undefined): string => {
  if (!fontFaces || fontFaces.length === 0) return '';

  const preloadHints = fontFaces.map(getFontLinkHeader).join('\n');
  const fontFaceRules = fontFaces.map(getFontFaceDefinition).join('\n');

  return `${preloadHints}\n<style>${fontFaceRules}</style>`;
};

export const getIframeHtml = (configType: ConfigType, config: AnyConfig) => {
  const { baseUrl, nonce } = config;
  const json = JSON.stringify(config);

  const fontHeader = getFontHeader(config.editorFont?.fontFaces);

  const safeNonceAttr = nonce ? `nonce="${escapeHtml(nonce)}"` : '';

  return `<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8" />
      <!-- Disable pinch zooming -->
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"
      />
      <meta id="gl-config-type" data-settings="${escapeHtml(configType)}" />
      <meta id="gl-config-json" data-settings="${escapeHtml(json)}" />
      ${fontHeader}
      <link
        data-name="vs/workbench/workbench.web.main"
        rel="stylesheet"
        href="${escapeHtml(baseUrl)}/vscode/out/vs/workbench/workbench.web.main.css"
      />
    </head>
    <body>
      <script src="${escapeHtml(baseUrl)}/main.js" ${safeNonceAttr}></script>
    </body>
  </html>`;
};
