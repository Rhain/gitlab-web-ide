import { createFullConfig, createClientOnlyConfig, waitForPromises } from '@gitlab/utils-test';
import type { AnyConfig, ClientOnlyConfig } from '@gitlab/web-ide-types';
import { createOAuthClient } from '@gitlab/oauth-client';
import { oauthCallback, start, startRemote } from './index';
import { getIframeHtml } from './getIframeHtml';
import type { IUnloadPreventer } from './unloadPreventer';
import { createUnloadPreventer } from './unloadPreventer';
import handleVSCodeTrackingMessage from './handleVSCodeTrackingMessage';
import { checkOAuthToken } from './checkOAuthToken';

import '@gitlab/utils-test/src/jsdom.d';

jest.mock('./checkOAuthToken');
jest.mock('./getIframeHtml');
jest.mock('./unloadPreventer');
jest.mock('./handleVSCodeTrackingMessage');
jest.mock('@gitlab/oauth-client');

const TEST_OAUTH_CLIENT: ReturnType<typeof createOAuthClient> = {
  checkForValidToken: jest.fn().mockResolvedValue(true),
  getToken: jest.fn().mockRejectedValue(undefined),
  handleCallback: jest.fn().mockResolvedValue(undefined),
  redirectToAuthorize: jest.fn().mockResolvedValue(undefined),
};

describe('web-ide/src/index', () => {
  let parentElement: Element;
  let unloadPreventerMock: IUnloadPreventer;
  const config: AnyConfig = createFullConfig();
  const clientOnlyConfig: ClientOnlyConfig = createClientOnlyConfig();
  const iframeHtml = '<html><body>web ide assets</body></html>';
  const getIframe = () => document.querySelector('iframe');
  const postMessage = (key: string, params = {}) => {
    getIframe()?.contentWindow?.postMessage({ key, params }, window.location.origin);
  };

  const waitUntilMockIsInvoked = (mockFn: jest.Mock) =>
    new Promise<void>(resolve => {
      mockFn.mockImplementationOnce(() => resolve());
    });

  // why: Update JSDOM URL so that iframe postMessage origins match
  dom.reconfigure({ url: config.baseUrl });

  beforeEach(() => {
    parentElement = document.createElement('div');

    document.body.append(parentElement);

    unloadPreventerMock = {
      setShouldPrevent: jest.fn(),
      dispose: jest.fn(),
    };
    jest.mocked(createUnloadPreventer).mockReturnValueOnce(unloadPreventerMock);

    jest.mocked(createOAuthClient).mockReturnValue(TEST_OAUTH_CLIENT);
  });

  afterEach(() => {
    parentElement.remove();
  });

  describe.each`
    startFn        | methodName
    ${start}       | ${'start'}
    ${startRemote} | ${'startRemote'}
  `('$methodName', ({ startFn }) => {
    let dispose: () => void;

    beforeEach(async () => {
      (getIframeHtml as jest.Mock).mockReturnValueOnce(iframeHtml);
      ({ dispose } = await startFn(parentElement, config));
    });

    it('creates an iframe', () => {
      expect(getIframe()?.src).toBe('https://foo.bar/assets/placeholder.html');
    });

    it('adds the iframe HTML to the iframe when loaded', () => {
      expect(getIframe()?.contentWindow?.document.body).toBe(null);

      // Test that we are resilient to `load` being triggeres multiple times.
      getIframe()?.dispatchEvent(new Event('load'));
      getIframe()?.dispatchEvent(new Event('load'));

      expect(getIframe()?.contentWindow?.document.body.innerHTML).toBe('web ide assets');
    });

    describe('when iframe is loaded', () => {
      beforeEach(() => {
        getIframe()?.dispatchEvent(new Event('load'));
      });

      it.each`
        message           | params       | handler                     | handlerName
        ${'start-remote'} | ${{}}        | ${config.handleStartRemote} | ${'handleStartRemote'}
        ${'error'}        | ${{}}        | ${config.handleError}       | ${'handleError'}
        ${'close'}        | ${undefined} | ${config.handleClose}       | ${'handleClose'}
      `(
        'calls $handlerName when iframe posts $message message',
        async ({ message, params, handler }) => {
          postMessage(message, params);

          await waitUntilMockIsInvoked(handler);

          if (params) {
            expect(handler).toHaveBeenCalledWith(params);
          } else {
            expect(handler).toHaveBeenCalled();
          }
        },
      );

      describe('when iframe receives web-ide-tracking message', () => {
        it('calls handleTracking handler and passes the message event parameter', async () => {
          const params = { event: { name: 'connect-to-remote' } };

          postMessage('web-ide-tracking', params);

          await waitUntilMockIsInvoked(config.handleTracking as jest.Mock);

          expect(config.handleTracking).toHaveBeenCalledWith(params.event);
        });
      });

      describe('when iframe receives vscode-tracking message', () => {
        it('calls handleVSCodeTrackingEvent function', async () => {
          const params = { event: { name: 'remote-connection-successful' } };

          postMessage('vscode-tracking', params);

          await waitUntilMockIsInvoked(handleVSCodeTrackingMessage as jest.Mock);

          expect(handleVSCodeTrackingMessage).toHaveBeenCalledWith(params, config);
        });
      });

      describe('on dispose', () => {
        it('removes iframe', () => {
          expect(getIframe()).not.toBe(null);

          dispose();

          expect(getIframe()).toBe(null);
        });

        it('disposes unloadPreventer', () => {
          dispose();

          expect(unloadPreventerMock.dispose).toHaveBeenCalled();
        });
      });
    });
  });

  describe('when iframe errors', () => {
    const ERROR_EVENT = new ErrorEvent('error');

    let ready: Promise<void>;

    beforeEach(async () => {
      ({ ready } = await start(parentElement, clientOnlyConfig));

      getIframe()?.dispatchEvent(ERROR_EVENT);
    });

    it('rejects "ready"', async () => {
      await expect(ready).rejects.toEqual(ERROR_EVENT);
    });
  });

  describe('start', () => {
    it('resolves "ready" when "ready" message has been posted', async () => {
      const { ready } = await start(parentElement, clientOnlyConfig);

      // Test that it doesn't matter if a random "error" happens after "load"
      getIframe()?.dispatchEvent(new Event('load'));
      getIframe()?.dispatchEvent(new Event('error'));
      const isReadySpy = jest.fn();

      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      ready.then(isReadySpy);

      expect(isReadySpy).not.toHaveBeenCalled();

      postMessage('ready');

      await expect(ready).resolves.toBeUndefined();
      expect(isReadySpy).toHaveBeenCalled();
    });

    it('calls and waits for checkOAuthToken', async () => {
      const isFulfilledSpy = jest.fn();
      jest.mocked(checkOAuthToken).mockReturnValue(new Promise(() => {}));

      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      start(parentElement, clientOnlyConfig).finally(isFulfilledSpy);
      await waitForPromises();

      expect(checkOAuthToken).toHaveBeenCalled();
      expect(isFulfilledSpy).not.toHaveBeenCalled();
    });
  });

  describe('prevent-unload message', () => {
    beforeEach(async () => {
      await start(parentElement, clientOnlyConfig);

      getIframe()?.dispatchEvent(new Event('load'));
    });

    it.each([true, false])('updates uploadPreventer shouldPrevent state', async shouldPrevent => {
      postMessage('prevent-unload', {
        shouldPrevent,
      });

      await waitUntilMockIsInvoked(unloadPreventerMock.setShouldPrevent as jest.Mock);

      expect(unloadPreventerMock.setShouldPrevent).toHaveBeenCalledWith(shouldPrevent);
    });
  });

  describe('oauthCallback', () => {
    it('throws when called with something other than auth.type "oauth"', async () => {
      await expect(oauthCallback(clientOnlyConfig)).rejects.toThrowError(
        /Expected config.auth to be OAuth config./,
      );
    });

    it('calls handleCallback of OAuthClient', async () => {
      const oauthConfig: ClientOnlyConfig = {
        ...clientOnlyConfig,
        auth: {
          type: 'oauth',
          clientId: '123456',
          callbackUrl: 'https://example.com/oauth_callback',
        },
      };

      expect(createOAuthClient).not.toHaveBeenCalled();
      expect(TEST_OAUTH_CLIENT.handleCallback).not.toHaveBeenCalled();

      await oauthCallback(oauthConfig);

      expect(createOAuthClient).toHaveBeenCalledTimes(1);
      expect(createOAuthClient).toHaveBeenCalledWith({
        gitlabUrl: oauthConfig.gitlabUrl,
        oauthConfig: oauthConfig.auth,
      });
      expect(TEST_OAUTH_CLIENT.handleCallback).toHaveBeenCalledTimes(1);
    });
  });
});
