import {
  DefaultGitLabClient,
  DeprecatedGitLabClient as GitLabClient,
} from '@gitlab/gitlab-api-client';
import {
  COMMAND_FETCH_BUFFER_FROM_API,
  COMMAND_FETCH_FROM_API,
  COMMAND_MEDIATOR_TOKEN,
} from '@gitlab/web-ide-interop';
import type { ICommand, VSBufferWrapper } from '../types';
import {
  createCommands,
  COMMAND_START,
  COMMAND_FETCH_FILE_RAW,
  COMMAND_READY,
  COMMAND_START_REMOTE,
  COMMAND_OPEN_URI,
  COMMAND_COMMIT,
  COMMAND_PREVENT_UNLOAD,
  COMMAND_SET_HREF,
  COMMAND_TRACK_EVENT,
  COMMAND_FETCH_PROJECT_BRANCHES,
  COMMAND_CREATE_PROJECT_BRANCH,
  COMMAND_FETCH_MERGE_REQUEST_DIFF_STATS,
} from './index';
import * as commit from './commit';
import * as start from './start';
import * as fetchFileRaw from './fetchFileRaw';
import { postMessage } from './utils/postMessage';
import { generateUniqueToken } from './utils/generateUniqueToken';
import { TEST_COMMIT_PAYLOAD, TEST_CONFIG, createMockLocation } from '../../test-utils';

jest.mock('@gitlab/gitlab-api-client');
jest.mock('./commit');
jest.mock('./start');
jest.mock('./fetchFileRaw');
jest.mock('./utils/postMessage');
jest.mock('./utils/generateUniqueToken');

const TEST_BUFFER_WRAPPER: VSBufferWrapper = (buffer: Uint8Array) => ({
  buffer,
});

const TEST_MEDIATOR_TOKEN = 'fake-mediator-token';

describe('vscode-mediator-commands/commands', () => {
  let commitSpy: jest.Mock;
  let startSpy: jest.Mock;
  let fetchFileRawSpy: jest.Mock;
  let commands: ICommand[] = [];

  const callCommand = (id: string, ...args: unknown[]) => {
    const command = commands.find(x => x.id === id);

    if (!command) {
      return Promise.reject(new Error('TEST_ERROR: Command not found'));
    }

    return (command.handler as (...x: unknown[]) => unknown).call(null, ...args);
  };

  const createCommandSpy = (commandFactoryModule: { commandFactory: unknown }) => {
    const spy = jest.fn();

    (commandFactoryModule.commandFactory as jest.Mock).mockImplementation(
      (...dependencies: unknown[]) =>
        (...args: unknown[]) =>
          spy({
            dependencies,
            args,
          }),
    );

    return spy;
  };

  const getClient = () => jest.mocked(GitLabClient).mock.instances[0];
  const getDefaultClient = () => jest.mocked(jest.mocked(DefaultGitLabClient).mock.instances[0]);

  beforeEach(async () => {
    // createCommandSpy sets up the module mock too, so we have to call this before createCommands
    commitSpy = createCommandSpy(commit);
    startSpy = createCommandSpy(start);
    fetchFileRawSpy = createCommandSpy(fetchFileRaw);

    jest.mocked(generateUniqueToken).mockResolvedValue(TEST_MEDIATOR_TOKEN);

    commands = await createCommands(TEST_CONFIG, TEST_BUFFER_WRAPPER);
  });

  describe(COMMAND_START, () => {
    beforeEach(async () => {
      await callCommand(COMMAND_START, TEST_MEDIATOR_TOKEN, 'arg');
    });

    it('triggers start command', () => {
      expect(startSpy).toHaveBeenCalledWith({
        dependencies: [TEST_CONFIG, expect.any(GitLabClient)],
        args: ['arg'],
      });
    });
  });

  describe(COMMAND_FETCH_FILE_RAW, () => {
    beforeEach(async () => {
      await callCommand(COMMAND_FETCH_FILE_RAW, TEST_MEDIATOR_TOKEN, 'ref', 'path');
    });

    it('triggers fetchFileRaw command', () => {
      expect(fetchFileRawSpy).toHaveBeenCalledWith({
        dependencies: [TEST_CONFIG, expect.any(GitLabClient), TEST_BUFFER_WRAPPER],
        args: ['ref', 'path'],
      });
    });
  });

  describe.each<[string, keyof GitLabClient, unknown]>([
    [COMMAND_FETCH_MERGE_REQUEST_DIFF_STATS, 'fetchMergeRequestDiffStats', { mergeRequestId: '7' }],
    [
      COMMAND_FETCH_PROJECT_BRANCHES,
      'fetchProjectBranches',
      { projectPath: 'gitlab-org/gitlab', searchPattern: '*foo*', offset: 0, limit: 100 },
    ],
    [
      COMMAND_CREATE_PROJECT_BRANCH,
      'createProjectBranch',
      { projectPath: 'gitlab-org/gitlab', searchPattern: '*foo*', offset: 0, limit: 100 },
    ],
  ])('%s', (command, clientMethodName, payload) => {
    it('calls client with payload', async () => {
      await callCommand(command, TEST_MEDIATOR_TOKEN, payload);

      expect(getClient()[clientMethodName]).toHaveBeenCalledWith(payload);
    });
  });

  describe(COMMAND_FETCH_FROM_API, () => {
    it('calls client with payload', async () => {
      const payload = { type: 'api', mehthod: 'GET', path: 'test' };
      const expectedReturn = { test: 123 };
      getDefaultClient().fetchFromApi.mockResolvedValue(expectedReturn);

      const actual = await callCommand(COMMAND_FETCH_FROM_API, TEST_MEDIATOR_TOKEN, payload);

      expect(getDefaultClient().fetchFromApi).toHaveBeenCalledWith(payload);
      expect(actual).toBe(expectedReturn);
    });

    it('rejects if called without token', async () => {
      const payload = { type: 'api', mehthod: 'GET', path: 'test' };

      await expect(callCommand(COMMAND_FETCH_FROM_API, payload)).rejects.toThrow('Token invalid');
    });
  });

  describe(COMMAND_FETCH_BUFFER_FROM_API, () => {
    it('calls client with payload', async () => {
      const payload = { type: 'api', mehthod: 'GET', path: 'test' };
      const expectedByteArray = new Uint8Array(new TextEncoder().encode('hello world'));
      getDefaultClient().fetchBufferFromApi.mockResolvedValue(expectedByteArray.buffer);

      const actual = await callCommand(COMMAND_FETCH_BUFFER_FROM_API, TEST_MEDIATOR_TOKEN, payload);

      expect(getDefaultClient().fetchBufferFromApi).toHaveBeenCalledWith(payload);
      expect(actual).toEqual({ buffer: expectedByteArray });
    });
  });

  describe(COMMAND_READY, () => {
    beforeEach(async () => {
      await callCommand(COMMAND_READY, TEST_MEDIATOR_TOKEN);
    });

    it('triggers postMessage', () => {
      expect(postMessage).toHaveBeenCalledWith({ key: 'ready' });
    });
  });

  describe(COMMAND_START_REMOTE, () => {
    beforeEach(async () => {
      await callCommand(COMMAND_START_REMOTE, TEST_MEDIATOR_TOKEN, { connectionToken: '123' });
    });

    it('sends start-message postMessage', () => {
      expect(postMessage).toHaveBeenCalledWith({
        key: 'start-remote',
        params: { connectionToken: '123' },
      });
    });
  });

  describe(COMMAND_OPEN_URI, () => {
    beforeEach(() => {
      window.open = jest.fn();
    });

    it.each`
      key                  | uriValue
      ${'feedbackIssue'}   | ${TEST_CONFIG.links.feedbackIssue}
      ${'userPreferences'} | ${TEST_CONFIG.links.userPreferences}
    `('opens $uriValue in new window when uri key is $key', async ({ key, uriValue }) => {
      await callCommand(COMMAND_OPEN_URI, TEST_MEDIATOR_TOKEN, { key });

      expect(window.parent.open).toHaveBeenCalledWith(uriValue, '_blank', 'noopener,noreferrer');
    });
  });

  describe(COMMAND_COMMIT, () => {
    beforeEach(async () => {
      await callCommand(COMMAND_COMMIT, TEST_MEDIATOR_TOKEN, TEST_COMMIT_PAYLOAD);
    });

    it('triggers commit command', () => {
      expect(commitSpy).toHaveBeenCalledWith({
        dependencies: [TEST_CONFIG, expect.any(GitLabClient)],
        args: [TEST_COMMIT_PAYLOAD],
      });
    });
  });

  describe(COMMAND_PREVENT_UNLOAD, () => {
    beforeEach(async () => {
      await callCommand(COMMAND_PREVENT_UNLOAD, TEST_MEDIATOR_TOKEN, { shouldPrevent: true });
    });

    it('sends start-message postMessage', () => {
      expect(postMessage).toHaveBeenCalledWith({
        key: 'prevent-unload',
        params: { shouldPrevent: true },
      });
    });
  });

  describe(COMMAND_TRACK_EVENT, () => {
    beforeEach(async () => {
      await callCommand(COMMAND_TRACK_EVENT, TEST_MEDIATOR_TOKEN, [{ name: 'connect-remote' }]);
    });

    it('sends web-ide-tracking postMessage', () => {
      expect(postMessage).toHaveBeenCalledWith({
        key: 'web-ide-tracking',
        params: [{ name: 'connect-remote' }],
      });
    });
  });

  describe(COMMAND_SET_HREF, () => {
    const origWindowParent = window.parent;

    beforeEach(() => {
      window.parent = { ...window };
      const mockLocation = createMockLocation();
      Object.defineProperty(window.parent, 'location', {
        get() {
          return mockLocation;
        },
      });
    });

    afterEach(() => {
      window.parent = origWindowParent;
    });

    it.each`
      origHref                    | href                                   | expectHref
      ${'http://localhost/'}      | ${'/new/path/place'}                   | ${'http://localhost/new/path/place'}
      ${'http://localhost/-/ide'} | ${'/new/path/place'}                   | ${'http://localhost/new/path/place'}
      ${'http://localhost/-/ide'} | ${'http://example.org/new/path/place'} | ${'http://example.org/new/path/place'}
    `(
      'with origHref=$origHref and href=$href, updates window.parent.location',
      async ({ origHref, href, expectHref }) => {
        window.parent.location.href = origHref;

        await callCommand(COMMAND_SET_HREF, TEST_MEDIATOR_TOKEN, href);

        expect(window.parent.location.href).toBe(expectHref);
      },
    );
  });

  describe(COMMAND_MEDIATOR_TOKEN, () => {
    it('returns the generated mediator token', async () => {
      expect(callCommand(COMMAND_MEDIATOR_TOKEN)).toEqual(TEST_MEDIATOR_TOKEN);
    });
  });
});
