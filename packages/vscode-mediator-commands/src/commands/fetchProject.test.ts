import type { DeprecatedGitLabClient as GitLabClient } from '@gitlab/gitlab-api-client';
import { createTestClient, createTestProject, TEST_CONFIG } from '../../test-utils';
import type { GitLabProject } from '../types';
import * as fetchProject from './fetchProject';

jest.mock('@gitlab/gitlab-api-client');

const TEST_PROJECT = createTestProject(TEST_CONFIG.projectPath);

describe('vscode-mediator-commands/commands/fetchProject', () => {
  let client: jest.Mocked<GitLabClient>;
  let command: fetchProject.FetchProjectCommand;
  let result: GitLabProject;

  beforeEach(async () => {
    client = createTestClient() as jest.Mocked<GitLabClient>;

    client.fetchProject.mockResolvedValue(TEST_PROJECT);
    command = fetchProject.commandFactory(TEST_CONFIG, client);

    result = await command();
  });

  it('returns result', () => {
    expect(result).toEqual(TEST_PROJECT);
  });

  it('calls clients', () => {
    expect(client.fetchProject).toHaveBeenCalledWith(TEST_CONFIG.projectPath);
  });
});
