import type { gitlab, DeprecatedGitLabClient as GitLabClient } from '@gitlab/gitlab-api-client';
import type { IFullConfig } from '../types';

export const commandFactory =
  (config: IFullConfig, client: GitLabClient) =>
  async (payload: gitlab.CommitPayload): Promise<void> => {
    await client.commit(config.projectPath, payload);
  };
