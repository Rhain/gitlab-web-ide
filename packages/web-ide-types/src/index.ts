export * from './config';
export * from './error';
export * from './messages';
export * from './instrumentation';

/**
 * This is the type we return from the `start` functions of the `@gitlab/web-ide` package
 */
export interface WebIde {
  /**
   * Promise that resolves when the Web IDE is actually ready.
   *
   * The Web IDE is ready when the runtime and relevant file tree is
   * loaded and ready for the user's interaction.
   */
  readonly ready: Promise<void>;

  /**
   * Disposes and cleans up the Web IDE instance.
   */
  dispose(): void;
}
