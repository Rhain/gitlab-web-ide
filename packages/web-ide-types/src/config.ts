import type { ErrorMessage, StartRemoteMessage } from './messages';
import type { TrackingEvent } from './instrumentation';

export interface OAuthConfig {
  type: 'oauth';

  /**
   * Client ID of the OAuth application
   */
  clientId: string;

  /**
   * Preconfigured Callback URL (aka Redirect URI) for the OAuth application
   */
  callbackUrl: string;

  /**
   * (Optional) Flag for whether the refresh token should be persisted or not.
   *
   * If the flag is false, we still persist the access token, but we will
   * attempt to silently reauthorize if the token is about to expire.
   */
  protectRefreshToken?: boolean;

  /**
   * (Optional) overwrites the default `expiresIn` for the token whenever
   * a new one is granted. Helpful for testing refresh behavior.
   */
  tokenLifetime?: number;
}

export interface TokenAuthConfig {
  type: 'token';

  /**
   * Access token for the account
   */
  token: string;
}

export type AuthConfig = OAuthConfig | TokenAuthConfig;
export type AuthType = AuthConfig['type'];

export interface BaseConfigLinks {
  feedbackIssue: string;
  userPreferences: string;
  signIn: string;
}

interface FontSrc {
  /**
    Contains the full URL of the font resource.
    It's used in the preload link and in the @font-face/src/url CSS attribute.
   */
  url: string;

  /**
    Specifies the mime type in preload link (type='font/${format}') and
    in the @font-face/src/format CSS attribute.
    For available values, check https://www.iana.org/assignments/media-types/media-types.xhtml#font
  */
  format: string;
}

/**
 * A font face definition based on the FontFace interface, which is itself
 * a representation of the @font-face CSS rule.
 *
 * Values here are mostly used to construct @font-face CSS rules, but also for
 * preload hints and the editor font family setting.
 *
 * We set most properties of FontFace to be optional, and we also omit some:
 *  - `load`, `loaded` and `status` aren't relevant to the CSS rule;
 *  - `variant` is no longer a CSS descriptor, but a value property (i.e., not
 *     to be used in @font-face rules).
 *
 * See also:
 * - https://drafts.csswg.org/css-font-loading/#fontface-interface for more
 *   details about possible properties.
 * - https://developer.mozilla.org/en-US/docs/Glossary/CSS_Descriptor
 */
export interface WebIDEFontFace
  extends Omit<Partial<FontFace>, 'load' | 'loaded' | 'status' | 'variant'> {
  /**
    Used in the VS Code settings.json (e.g. as 'editor.fontFamily').
    It will also be used in the @font-face/font-family CSS attribute.
    The `Partial` makes it optional, so override it here to make it required.
  */
  family: FontFace['family'];

  src: Array<FontSrc>;
}

export interface BaseConfig {
  baseUrl: string;
  nonce?: string;
  handleError?: (params: ErrorMessage['params']) => void;
  handleStartRemote?: (params: StartRemoteMessage['params']) => void;
  handleClose?: () => void;
  handleTracking?: (event: TrackingEvent) => void;
  links: BaseConfigLinks;
  /** Contains fonts to be used in VS Code Text Editors */
  editorFont?: {
    fallbackFontFamily: string;
    fontFaces: WebIDEFontFace[];
  };
}

export interface ForkInfo {
  // This will be truthy if the fork exists
  ide_path?: string;

  // This will be truthy if the fork doesn't exist and the user can fork
  fork_path?: string;
}

export interface ClientOnlyConfig extends BaseConfig {
  // gitlabUrl - The URL of the GitLab instance
  gitlabUrl: string;

  // projectPath - The path_with_namespace of the project to open
  projectPath: string;

  // codeSuggestionsEnabled - Boolean for whether code suggestion feature should be enabled
  codeSuggestionsEnabled?: boolean;

  // auth - Configuration for authentication. If not provided, the Web IDE will be read-only.
  auth?: AuthConfig;

  // filePath - Default file path to open
  filePath?: string;

  // mrId - If opening from an MR, the ID of the MR
  mrId?: string;

  // mrTargetProject - If opening from an MR, the project path of the MR
  mrTargetProject?: string;

  // ref - If not coming from an MR, the branch ref to open
  ref?: string;

  // httpHeaders - Extra headers to pass with api requests (for example, csrf headers)
  httpHeaders?: Record<string, string>;

  // forkInfo - Fork information for the given projectPath. This follows the pre-existing
  //            interface used in the old Web IDE
  //            https://gitlab.com/gitlab-org/gitlab/-/blob/dd1e70d3676891025534dc4a1e89ca9383178fe7/app/assets/javascripts/ide/stores/getters.js#L24
  forkInfo?: ForkInfo;

  // username - The current username for the GitLab context. This is used for things like
  //            generating default branch names.
  //            https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/82
  username?: string;

  // telemetryEnabled - This property is a boolean that indicates if telemetry is enabled
  //                      and bases its value on the web browser's do not track signal.
  //                    The single source of truth to determine if telemetry is enabled in
  //                    in the Web IDE is GitLab's tracking module.
  //                    See https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/assets/javascripts/tracking/tracker.js#L26
  telemetryEnabled?: boolean;
}
export interface RemoteConfig extends BaseConfig {
  remoteAuthority: string;
  connectionToken: string;
  hostPath: string;
}

/**
 * Config used for the oauthCallback entrypoint of the Web IDE.
 *
 * NOTE: Not used yet. See https://gitlab.com/gitlab-org/gitlab-web-ide/-/merge_requests/240 for upcoming changes.
 */
export type OAuthCallbackConfig = Pick<ClientOnlyConfig, 'gitlabUrl' | 'auth' | 'username'>;

export type AnyConfig = ClientOnlyConfig | RemoteConfig;

export type ConfigType = 'client-only' | 'remote';
