import type { IFileList } from '@gitlab/web-ide-fs';
import fuzzaldrinPlus from 'fuzzaldrin-plus';
import type { IFileSearcher } from './types';

export class FileSearcher implements IFileSearcher {
  readonly #fileList;

  constructor(fileList: IFileList) {
    this.#fileList = fileList;
  }

  async searchBlobPaths(term: string, maxResults = 0): Promise<string[]> {
    const paths = await this.#fileList.listAllBlobs();

    return fuzzaldrinPlus.filter(paths, term, {
      maxResults: maxResults <= 0 ? 10 : maxResults,
    });
  }
}
