import * as vscode from 'vscode';
import { memoize } from 'lodash';
import { COMMAND_GET_CONFIG, COMMAND_MEDIATOR_TOKEN } from '@gitlab/web-ide-interop';
import {
  COMMAND_START,
  COMMAND_FETCH_FILE_RAW,
  COMMAND_READY,
  COMMAND_START_REMOTE,
  COMMAND_COMMIT,
  COMMAND_PREVENT_UNLOAD,
  COMMAND_SET_HREF,
  COMMAND_FETCH_MERGE_REQUEST_DIFF_STATS,
  COMMAND_FETCH_PROJECT_BRANCHES,
  COMMAND_CREATE_PROJECT_BRANCH,
  COMMAND_OPEN_URI,
} from '@gitlab/vscode-mediator-commands';
import type {
  StartCommandResponse,
  VSCodeBuffer,
  StartRemoteMessageParams,
  PreventUnloadMessageParams,
  FetchMergeRequestDiffStatsParams,
  FetchMergeRequestDiffStatsResponse,
  FetchProjectBranchesParams,
  FetchProjectBranchesResponse,
  CreateProjectBranchParams,
  CreateProjectBranchResponse,
  GitLabCommitPayload,
  StartCommandOptions,
  IFullConfig,
} from '@gitlab/vscode-mediator-commands';
import type { BaseConfigLinks } from '@gitlab/web-ide-types';

const getMediatorToken = memoize(() =>
  vscode.commands.executeCommand<string>(COMMAND_MEDIATOR_TOKEN),
);

const executeMediatorCommand = async <T = unknown>(
  commandId: string,
  ...args: unknown[]
): Promise<T> => {
  const token = await getMediatorToken();

  return vscode.commands.executeCommand<T>(commandId, token, ...args);
};

export const start = async (options: StartCommandOptions = {}): Promise<StartCommandResponse> =>
  executeMediatorCommand(COMMAND_START, options);

export const fetchFileRaw = async (ref: string, path: string): Promise<VSCodeBuffer> =>
  executeMediatorCommand(COMMAND_FETCH_FILE_RAW, ref, path);

export const fetchMergeRequestDiffStats = async (
  params: FetchMergeRequestDiffStatsParams,
): Promise<FetchMergeRequestDiffStatsResponse> =>
  executeMediatorCommand(COMMAND_FETCH_MERGE_REQUEST_DIFF_STATS, params);

export const fetchProjectBranches = (
  params: FetchProjectBranchesParams,
): Promise<FetchProjectBranchesResponse> =>
  executeMediatorCommand(COMMAND_FETCH_PROJECT_BRANCHES, params);

export const createProjectBranch = (
  params: CreateProjectBranchParams,
): Promise<CreateProjectBranchResponse> =>
  executeMediatorCommand(COMMAND_CREATE_PROJECT_BRANCH, params);

export const ready = () => executeMediatorCommand(COMMAND_READY);

export const startRemote = (params: StartRemoteMessageParams) =>
  executeMediatorCommand(COMMAND_START_REMOTE, params);

export const commit = (payload: GitLabCommitPayload) =>
  executeMediatorCommand(COMMAND_COMMIT, payload);

export const preventUnload = (params: PreventUnloadMessageParams) =>
  executeMediatorCommand(COMMAND_PREVENT_UNLOAD, params);

export const setHref = (href: string) => executeMediatorCommand(COMMAND_SET_HREF, href);

export const openUri = (options: { key: keyof BaseConfigLinks }) =>
  executeMediatorCommand(COMMAND_OPEN_URI, options);

// why: This is memoized because the config should never ever change
export const getConfig = memoize(() => executeMediatorCommand<IFullConfig>(COMMAND_GET_CONFIG));
