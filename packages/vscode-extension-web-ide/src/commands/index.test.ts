import * as vscode from 'vscode';
import {
  START_REMOTE_COMMAND_ID,
  CHECKOUT_BRANCH_COMMAND_ID,
  GO_TO_GITLAB_COMMAND_ID,
  GO_TO_PROJECT_COMMAND_ID,
  SHARE_YOUR_FEEDBACK_COMMAND_ID,
  OPEN_REMOTE_WINDOW_COMMAND_ID,
  RELOAD_WITH_WARNING_COMMAND_ID,
  COMPARE_WITH_MR_BASE_COMMAND_ID,
} from '../constants';
import { registerCommands } from './index';
import checkoutBranch from './checkoutBranch';
import compareWithMrBase from './compareWithMrBase';
import goToGitLab from './goToGitLab';
import goToProject from './goToProject';
import reloadWithWarning from './reloadWithWarning';
import shareYourFeedback from './shareYourFeedback';
import startRemote from './startRemote';
import { TEST_COMMANDS_INITIAL_DATA } from '../../test-utils';

jest.mock('./goToProject');
jest.mock('./goToGitLab');
jest.mock('./checkoutBranch');

describe('commands/index', () => {
  describe('registerCommands', () => {
    let disposables: vscode.Disposable[];

    beforeEach(() => {
      disposables = [];
      jest
        .spyOn(vscode.commands, 'registerCommand')
        // Note: registerCommand has to return a Disposable, but we also add the commandName
        //       so that we can easily assert what kind of disposable was added to the array
        .mockImplementation(commandName => ({ commandName, dispose: () => 'noop' }));
    });

    it.each`
      commandName                        | commandFn
      ${START_REMOTE_COMMAND_ID}         | ${startRemote}
      ${SHARE_YOUR_FEEDBACK_COMMAND_ID}  | ${shareYourFeedback}
      ${RELOAD_WITH_WARNING_COMMAND_ID}  | ${reloadWithWarning}
      ${COMPARE_WITH_MR_BASE_COMMAND_ID} | ${compareWithMrBase}
    `(
      'registers $commandName command in the vscode command registry',
      ({ commandName, commandFn }) => {
        registerCommands(disposables, Promise.resolve(TEST_COMMANDS_INITIAL_DATA));

        expect(vscode.commands.registerCommand).toHaveBeenCalledWith(commandName, commandFn);
      },
    );

    it.each`
      commandName                   | commandFn
      ${GO_TO_GITLAB_COMMAND_ID}    | ${goToGitLab}
      ${GO_TO_PROJECT_COMMAND_ID}   | ${goToProject}
      ${CHECKOUT_BRANCH_COMMAND_ID} | ${checkoutBranch}
    `(
      'provides command initial data and registers $commandName command in the vscode command registry',
      ({ commandName, commandFn }) => {
        const noop = () => true;
        (commandFn as jest.Mock).mockReturnValueOnce(noop);

        registerCommands(disposables, Promise.resolve(TEST_COMMANDS_INITIAL_DATA));

        expect(commandFn).toHaveBeenCalledWith(Promise.resolve(TEST_COMMANDS_INITIAL_DATA));

        expect(vscode.commands.registerCommand).toHaveBeenCalledWith(commandName, noop);
      },
    );

    it('registers commands in disposables', () => {
      registerCommands(disposables, Promise.resolve(TEST_COMMANDS_INITIAL_DATA));

      expect(disposables).toEqual([
        { commandName: CHECKOUT_BRANCH_COMMAND_ID, dispose: expect.any(Function) },
        { commandName: COMPARE_WITH_MR_BASE_COMMAND_ID, dispose: expect.any(Function) },
        { commandName: GO_TO_GITLAB_COMMAND_ID, dispose: expect.any(Function) },
        { commandName: GO_TO_PROJECT_COMMAND_ID, dispose: expect.any(Function) },
        { commandName: OPEN_REMOTE_WINDOW_COMMAND_ID, dispose: expect.any(Function) },
        { commandName: SHARE_YOUR_FEEDBACK_COMMAND_ID, dispose: expect.any(Function) },
        { commandName: START_REMOTE_COMMAND_ID, dispose: expect.any(Function) },
        { commandName: RELOAD_WITH_WARNING_COMMAND_ID, dispose: expect.any(Function) },
      ]);
    });
  });
});
