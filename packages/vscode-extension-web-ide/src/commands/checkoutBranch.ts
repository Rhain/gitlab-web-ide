import * as vscode from 'vscode';
import { uniq, flatten } from 'lodash';
import { RELOAD_WITH_WARNING_COMMAND_ID } from '../constants';
import { fetchProjectBranches, createProjectBranch } from '../mediator';
import type { CommandsInitialData } from '../types';
import { showSearchableQuickPick, showInputBox } from '../vscodeUi';

// region: types -------------------------------------------------------
interface NewBranchQuickPickItem extends vscode.QuickPickItem {
  type: 'new-branch';
}

interface NotFoundQuickPickItem extends vscode.QuickPickItem {
  type: 'not-found';
}

interface ExistingBranchQuickPickItem extends vscode.QuickPickItem {
  type: 'existing-branch';
  ref: string;
}

type BranchQuickPickItem =
  | NewBranchQuickPickItem
  | ExistingBranchQuickPickItem
  | NotFoundQuickPickItem;

// region: constants ---------------------------------------------------
export const ITEM_CREATE_NEW: NewBranchQuickPickItem = {
  type: 'new-branch',
  alwaysShow: true,
  label: '$(plus) Create new branch...',
};
export const ITEM_NOT_FOUND: NotFoundQuickPickItem = {
  type: 'not-found',
  alwaysShow: true,
  label: 'No branches found',
};

export const MSG_SELECT_BRANCH = 'Select a branch to checkout';
export const MSG_ERROR_SEARCH_BRANCH =
  'Error while searching for branches. Please try again or check the developer console for details.';

// region: helpers functions -------------------------------------------
const mapBranchNameToQuickPickItem = (branchName: string): ExistingBranchQuickPickItem => ({
  type: 'existing-branch',
  alwaysShow: false,
  label: `$(git-branch) ${branchName}`,
  ref: branchName,
});

const tryCreateProjectBranch = async (...args: Parameters<typeof createProjectBranch>) => {
  try {
    return await createProjectBranch(...args);
  } catch {
    return {
      errors: ['An unexpected error occurred while creating the branch. Please try again.'],
    };
  }
};

const validateAndSubmitBranchName = async (
  branchName: string,
  { ref, isEmptyRepo, projectPath }: { ref: string; isEmptyRepo: boolean; projectPath: string },
) => {
  if (!branchName) {
    return 'Branch name cannot be empty.';
  }

  // If we are in an empty repo, just accept the branch name
  // why: Creating a branch will actually make the repo non-empty so we just
  //      support the user "checking out" a local branch name
  // TODO: Maybe we should do some client-side validation here
  if (isEmptyRepo) {
    return undefined;
  }

  const { errors } = await tryCreateProjectBranch({
    name: branchName,
    projectPath,
    ref,
  });

  if (errors?.length) {
    return errors[0];
  }

  return undefined;
};

const showCreateBranchPrompt = async (
  options: CommandsInitialData,
): Promise<string | undefined> => {
  const { path_with_namespace: projectPath, empty_repo: isEmptyRepo } = options.project;
  const { sha: commitId } = options.ref;

  const result = await showInputBox({
    placeholder: 'Branch name',
    prompt: 'Please provide a new branch name',
    // TODO: This "validate" actually "submits" the branchName and returns
    //       any BE errors as a validation message.
    //       Maybe we should call this "showInputBox" option "trySubmit" or "validateSubmission".
    //       https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/105
    validate: branchName =>
      validateAndSubmitBranchName(branchName, { ref: commitId, isEmptyRepo, projectPath }),
  });

  if (result.canceled) {
    return undefined;
  }

  return result.value;
};

const loadRef = async (ref: string) => {
  await vscode.commands.executeCommand(RELOAD_WITH_WARNING_COMMAND_ID, {
    message: `Are you sure you want to checkout "${ref}"? Any unsaved changes will be lost.`,
    okText: 'Yes',
    ref,
  });
};

const getRefFromBranchSelection = async (
  selection: BranchQuickPickItem | undefined,
  options: CommandsInitialData,
): Promise<string | undefined> => {
  if (!selection) {
    return undefined;
  }

  if (selection.type === 'existing-branch') {
    return selection.ref;
  }

  if (selection.type === 'new-branch') {
    return showCreateBranchPrompt(options);
  }

  return undefined;
};

const handleSearchError = async (e: unknown): Promise<void> => {
  // eslint-disable-next-line no-console
  console.error('[gitlab-webide] Error occurred while searching for branches', e);

  await vscode.window.showWarningMessage(
    'Error while searching for branches. Please try again or check the developer console for details.',
  );
};

const searchBranchesFromPattern = async (searchPatternArg: string, projectPath: string) => {
  if (searchPatternArg) {
    const branchNames = await Promise.all([
      fetchProjectBranches({
        projectPath,
        searchPattern: searchPatternArg,
        offset: 0,
        limit: 1,
      }),
      fetchProjectBranches({
        projectPath,
        searchPattern: `*${searchPatternArg}*`,
        offset: 0,
        limit: 100,
      }),
    ]);

    return uniq(flatten(branchNames));
  }

  return fetchProjectBranches({
    projectPath,
    searchPattern: '*',
    offset: 0,
    limit: 100,
  });
};

const searchBranches = async (
  searchPatternArg: string,
  defaultItems: BranchQuickPickItem[],
  options: CommandsInitialData,
) => {
  const {
    project: { path_with_namespace: projectPath },
  } = options;

  const branchNames = await searchBranchesFromPattern(searchPatternArg, projectPath);

  const branchItems = branchNames.map(mapBranchNameToQuickPickItem);

  const result = [...defaultItems, ...branchItems];

  if (result.length) {
    return result;
  }

  return [ITEM_NOT_FOUND];
};

// region: command factory and handler ---------------------------------
export default (asyncOptions: Thenable<CommandsInitialData>) => async () => {
  const options = await asyncOptions;

  const defaultItems = options.userPermissions.pushCode ? [ITEM_CREATE_NEW] : [];

  const selection = await showSearchableQuickPick<BranchQuickPickItem>({
    placeholder: MSG_SELECT_BRANCH,
    defaultItems,
    searchItems: searchPattern => searchBranches(searchPattern, defaultItems, options),
    handleSearchError,
  });

  const ref = await getRefFromBranchSelection(selection, options);

  // what: User canceled something
  if (!ref) {
    return;
  }

  await loadRef(ref);
};
