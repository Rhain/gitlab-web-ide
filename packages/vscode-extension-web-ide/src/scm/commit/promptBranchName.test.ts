import * as vscode from 'vscode';
import type { IFullConfig } from '@gitlab/vscode-mediator-commands';
import { generateBranchName } from './generateBranchName';
import type { IBranchSelection } from './promptBranchName';
import { promptBranchName } from './promptBranchName';
import { getConfig } from '../../mediator';

jest.mock('./generateBranchName');
jest.mock('../../mediator');

const TEST_BRANCH = 'test-main';
const TEST_GENERATED_BRANCH = 'test-main-path-123';
const TEST_CONFIG: Partial<IFullConfig> = {
  username: 'loremuser',
};

describe('scm/commit/promptBranchName', () => {
  let resolveInputBox: (value: string | undefined) => void;
  let result: Promise<IBranchSelection | undefined>;

  beforeEach(() => {
    jest.spyOn(vscode.window, 'showInputBox').mockImplementation(
      () =>
        new Promise(resolve => {
          resolveInputBox = resolve;
        }),
    );

    jest.mocked(generateBranchName).mockReturnValue(TEST_GENERATED_BRANCH);
    jest.mocked(getConfig).mockResolvedValue(TEST_CONFIG as IFullConfig);

    result = promptBranchName(TEST_BRANCH);
  });

  it('shows input box requesting a branch name', () => {
    expect(vscode.window.showInputBox).toHaveBeenCalledWith({
      ignoreFocusOut: true,
      placeHolder: `Leave empty to use "${TEST_GENERATED_BRANCH}"`,
      title: 'Create a new branch',
    });
  });

  it('passes username to generateBranchName', () => {
    expect(generateBranchName).toHaveBeenCalledWith(TEST_BRANCH, TEST_CONFIG.username);
  });

  it.each`
    inputBoxResult        | expectation
    ${undefined}          | ${undefined}
    ${''}                 | ${{ isNewBranch: true, branchName: TEST_GENERATED_BRANCH }}
    ${'brand-new-branch'} | ${{ isNewBranch: true, branchName: 'brand-new-branch' }}
  `(
    'with inputBox resolves with $inputBoxResult, returns $expectation',
    async ({ inputBoxResult, expectation }) => {
      resolveInputBox(inputBoxResult);

      await expect(result).resolves.toEqual(expectation);
    },
  );
});
