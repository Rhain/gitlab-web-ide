import * as vscode from 'vscode';
import { joinPaths } from '@gitlab/utils-path';
import type { GitLabProject } from '@gitlab/vscode-mediator-commands';

// why: Export these for tests
export const ITEM_CREATE_MR = { title: 'Create MR' };
export const ITEM_GO_TO_MR = { title: 'Go to MR' };
export const ITEM_GO_TO_PROJECT = { title: 'Go to Project' };
export const ITEM_CONTINUE = { title: 'Continue working' };

const getNewMRUrl = (project: GitLabProject, branchName: string) => {
  const sourceBranch = branchName;
  // TODO: What if from fork? Can the forked project have a different default branch?
  const targetBranch = project.default_branch;

  const url = joinPaths(project.web_url, '-', 'merge_requests', 'new');

  // It looks like `nav_source`, `source_branch,` and `target_branch` are the only params we need to worry about
  // https://gitlab.com/gitlab-org/gitlab/-/blob/dd1e70d3676891025534dc4a1e89ca9383178fe7/app/assets/javascripts/ide/stores/utils.js#L128
  const newMrParams = [
    'nav_source=webide',
    `merge_request[source_branch]=${encodeURIComponent(sourceBranch)}`,
    `merge_request[target_branch]=${encodeURIComponent(targetBranch)}`,
  ].join('&');

  return `${url}?${newMrParams}`;
};

const shouldShowCreateMRItem = (project: GitLabProject, branchName: string) =>
  project.can_create_merge_request_in &&
  project.default_branch !== branchName &&
  !project.empty_repo;

const getMRActionItem = (project: GitLabProject, branchName: string, mrUrl?: string) => {
  if (mrUrl) {
    return [ITEM_GO_TO_MR];
  }

  if (shouldShowCreateMRItem(project, branchName)) {
    return [ITEM_CREATE_MR];
  }

  return [];
};

interface ShowSuccessMessageOptions {
  project: GitLabProject;
  branchName: string;
  mrUrl?: string;
}
export const showSuccessMessage = async ({
  project,
  branchName,
  mrUrl,
}: ShowSuccessMessageOptions) => {
  const items = [...getMRActionItem(project, branchName, mrUrl), ITEM_GO_TO_PROJECT, ITEM_CONTINUE];
  const selection = await vscode.window.showInformationMessage(
    'Success! Your changes have been committed.',
    ...items,
  );

  if (selection === ITEM_GO_TO_PROJECT) {
    await vscode.env.openExternal(vscode.Uri.parse(project.web_url));
  } else if (selection === ITEM_CREATE_MR) {
    await vscode.env.openExternal(vscode.Uri.parse(getNewMRUrl(project, branchName)));
  } else if (selection === ITEM_GO_TO_MR && mrUrl) {
    await vscode.env.openExternal(vscode.Uri.parse(mrUrl));
  }
};
