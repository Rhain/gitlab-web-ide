import * as vscode from 'vscode';
import type { GitLabRef, GitLabProject } from '@gitlab/vscode-mediator-commands';
import * as mediator from '../../mediator';
import { RELOAD_COMMAND_ID } from '../../constants';
import { generateCommitMessage } from './generateCommitMessage';
import { getCommitPayload } from './getCommitPayload';
import { showSuccessMessage } from './showSuccessMessage';
import { showCommitErrorMessage } from './showCommitErrorMessage';
import type { CommitCommand, IReadonlySourceControlViewModel } from '../types';
import type { LocalStorage } from '../../types';
import { getBranchSelection } from './getBranchSelection';

interface CommandFactoryOptions {
  readonly ref: GitLabRef;
  readonly localStorage: LocalStorage;
  readonly branchMergeRequestUrl: string;
  readonly commitId: string;
  readonly viewModel: IReadonlySourceControlViewModel;
  readonly project: GitLabProject;
}

/**
 * Factory for the "commit" command
 *
 * It's important the command itself doesn't take any arguments so that
 * it can be triggered by the user.
 */
export const factory =
  ({
    ref,
    branchMergeRequestUrl,
    commitId: startingSha,
    viewModel,
    project,
    localStorage,
  }: CommandFactoryOptions): CommitCommand =>
  async ({ shouldPromptBranchName } = { shouldPromptBranchName: false }) => {
    const status = viewModel.getStatus();

    if (!status.length) {
      await vscode.window.showInformationMessage('No changes found. Not able to commit.');
      return;
    }

    const branchSelection = await getBranchSelection({
      project,
      ref,
      shouldPromptBranchName,
      localStorage,
    });

    const commitMessage = viewModel.getCommitMessage() || generateCommitMessage(status);

    // User canceled the operation
    if (!branchSelection) {
      return;
    }

    const { branchName, isNewBranch } = branchSelection;

    try {
      await mediator.commit(
        getCommitPayload({
          status,
          commitMessage,
          branchName,
          isNewBranch,
          startingSha,
        }),
      );
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
      await showCommitErrorMessage(e);
      return;
    }

    // If we created from a new branch, don't use the MR URL
    const mrUrl = isNewBranch ? '' : branchMergeRequestUrl;

    // We just want to fire and forget here
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    showSuccessMessage({ project, branchName, mrUrl });

    await vscode.commands.executeCommand(RELOAD_COMMAND_ID, { ref: branchName });
  };
