import * as vscode from 'vscode';
import type { IFileStatus } from '@gitlab/web-ide-fs';
import type { GitLabProject, GitLabRef } from '@gitlab/vscode-mediator-commands';
import {
  COMMIT_COMMAND_ID,
  COMMIT_COMMAND_TEXT,
  SECONDARY_COMMIT_COMMAND_TEXT,
  SOURCE_CONTROL_ID,
  SOURCE_CONTROL_NAME,
  SOURCE_CONTROL_CHANGES_NAME,
  SOURCE_CONTROL_CHANGES_ID,
  FS_SCHEME,
} from '../constants';
import { ResourceDecorationProvider } from './ResourceDecorationProvider';
import { fromUriToScmUri } from './uri';
import { createStatusViewModel, toResourceState } from './status';
import type { ISourceControlViewModel } from './types';

interface SourceControlViewModelOptions {
  repoRoot: string;
  sourceControl: vscode.SourceControl;
  changesGroup: vscode.SourceControlResourceGroup;
  resourceDecorationProvider: ResourceDecorationProvider;
}

class SourceControlViewModel implements ISourceControlViewModel {
  readonly #repoRoot: string;

  readonly #sourceControl: vscode.SourceControl;

  readonly #changesGroup: vscode.SourceControlResourceGroup;

  readonly #resourceDecorationProvider: ResourceDecorationProvider;

  #lastStatus: IFileStatus[];

  constructor({
    repoRoot,
    sourceControl,
    changesGroup,
    resourceDecorationProvider,
  }: SourceControlViewModelOptions) {
    this.#repoRoot = repoRoot;
    this.#sourceControl = sourceControl;
    this.#changesGroup = changesGroup;
    this.#resourceDecorationProvider = resourceDecorationProvider;

    this.#lastStatus = [];
  }

  getStatus() {
    return this.#lastStatus;
  }

  getCommitMessage() {
    return this.#sourceControl.inputBox.value;
  }

  update(statuses: IFileStatus[]) {
    this.#lastStatus = statuses;

    const statusVms = statuses.map(x => createStatusViewModel(x, this.#repoRoot));

    // why: It might be important to update the resource decorations first before
    // adding the resources themselves to the resource group.
    this.#resourceDecorationProvider.update(statusVms);
    this.#changesGroup.resourceStates = statusVms.map(statusVm => toResourceState(statusVm));
  }
}

const sourceControlSecondaryCommand = {
  title: SECONDARY_COMMIT_COMMAND_TEXT,
  command: COMMIT_COMMAND_ID,
  arguments: [
    {
      shouldPromptBranchName: true,
    },
  ],
};

const getCommands = (ref: GitLabRef, project: GitLabProject) => {
  if (ref.type === 'branch') {
    return {
      primaryCommand: {
        command: COMMIT_COMMAND_ID,
        title: `${COMMIT_COMMAND_TEXT} to '${ref.branch.name}'`,
        arguments: [],
      },
      secondaryCommands: project.empty_repo ? undefined : [[sourceControlSecondaryCommand]],
    };
  }

  return {
    primaryCommand: sourceControlSecondaryCommand,
    secondaryCommands: undefined,
  };
};

const createVSCodeSourceControl = (
  disposables: vscode.Disposable[],
  ref: GitLabRef,
  project: GitLabProject,
) => {
  const sourceControl = vscode.scm.createSourceControl(SOURCE_CONTROL_ID, SOURCE_CONTROL_NAME);
  const { primaryCommand, secondaryCommands } = getCommands(ref, project);

  disposables.push(sourceControl);

  sourceControl.acceptInputCommand = primaryCommand;
  sourceControl.inputBox.enabled = true;
  sourceControl.inputBox.placeholder = 'Commit message';
  sourceControl.actionButton = {
    description: primaryCommand.title,
    enabled: true,
    secondaryCommands,
    command: primaryCommand,
  };
  sourceControl.quickDiffProvider = {
    provideOriginalResource(uri: vscode.Uri): vscode.Uri | undefined {
      if (uri.scheme === FS_SCHEME) {
        return fromUriToScmUri(uri, '');
      }

      return undefined;
    },
  };

  return sourceControl;
};

const createVSCodeChangesResourceGroup = (
  disposables: vscode.Disposable[],
  sourceControl: vscode.SourceControl,
) => {
  const changesGroup = sourceControl.createResourceGroup(
    SOURCE_CONTROL_CHANGES_ID,
    SOURCE_CONTROL_CHANGES_NAME,
  );
  changesGroup.hideWhenEmpty = false;
  disposables.push(changesGroup);

  return changesGroup;
};

const createResourceDecorationProvider = (disposables: vscode.Disposable[]) => {
  const resourceDecorationProvider = new ResourceDecorationProvider();

  disposables.push(
    vscode.window.registerFileDecorationProvider(
      resourceDecorationProvider.createVSCodeDecorationProvider(),
    ),
  );

  return resourceDecorationProvider;
};

export const createSourceControlViewModel = (
  disposables: vscode.Disposable[],
  repoRoot: string,
  ref: GitLabRef,
  project: GitLabProject,
): ISourceControlViewModel => {
  const sourceControl = createVSCodeSourceControl(disposables, ref, project);
  const changesGroup = createVSCodeChangesResourceGroup(disposables, sourceControl);
  const resourceDecorationProvider = createResourceDecorationProvider(disposables);

  return new SourceControlViewModel({
    repoRoot,
    sourceControl,
    changesGroup,
    resourceDecorationProvider,
  });
};
