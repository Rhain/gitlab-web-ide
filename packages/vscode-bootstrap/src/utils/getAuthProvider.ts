import type { AuthProvider } from '@gitlab/gitlab-api-client';
import { createPrivateTokenProvider } from '@gitlab/gitlab-api-client';
import { asOAuthProvider, createOAuthClient } from '@gitlab/oauth-client';
import type { ClientOnlyConfig } from '@gitlab/web-ide-types';

export const getAuthProvider = (config: ClientOnlyConfig): AuthProvider | undefined => {
  if (config.auth?.type === 'token') {
    return createPrivateTokenProvider(config.auth.token);
  }
  if (config.auth?.type === 'oauth') {
    const client = createOAuthClient({
      oauthConfig: config.auth,
      gitlabUrl: config.gitlabUrl,
      owner: config.username,
    });

    return asOAuthProvider(client);
  }

  return undefined;
};
