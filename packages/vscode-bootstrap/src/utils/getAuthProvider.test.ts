import { createClientOnlyConfig, useFakeBroadcastChannel } from '@gitlab/utils-test';
import type { AuthConfig, OAuthConfig } from '@gitlab/web-ide-types';
import { DefaultOAuthClient } from '@gitlab/oauth-client';
import { getAuthProvider } from './getAuthProvider';

jest.mock('@gitlab/oauth-client/src/OAuthClient');

const TEST_TOKEN_AUTH: AuthConfig = {
  type: 'token',
  token: 'lorem-ipsum-dolar',
};

const TEST_OAUTH_AUTH: OAuthConfig = {
  type: 'oauth',
  callbackUrl: 'https://example.com/callback_url',
  clientId: '123456',
};

const TEST_OAUTH_TOKEN = 'test-secret-oauth-token';

describe('utils/getAuthProvider', () => {
  useFakeBroadcastChannel();

  it('returns undefined with undefined auth', () => {
    const provider = getAuthProvider({ ...createClientOnlyConfig(), auth: undefined });

    expect(provider).toBeUndefined();
  });

  it('returns token provider with token auth', async () => {
    const provider = getAuthProvider({ ...createClientOnlyConfig(), auth: TEST_TOKEN_AUTH });

    expect(provider).not.toBeUndefined();
    await expect(provider?.getHeaders()).resolves.toEqual({
      'PRIVATE-TOKEN': TEST_TOKEN_AUTH.token,
    });
  });

  it('returns oauth provider with oauth', async () => {
    const provider = getAuthProvider({ ...createClientOnlyConfig(), auth: TEST_OAUTH_AUTH });

    const oauthClientInstance = jest.mocked(DefaultOAuthClient).mock.instances[0];

    jest.mocked(oauthClientInstance.getToken).mockResolvedValue({
      accessToken: TEST_OAUTH_TOKEN,
      expiresAt: 0,
    });

    expect(provider).not.toBeUndefined();
    await expect(provider?.getHeaders()).resolves.toEqual({
      Authorization: `Bearer ${TEST_OAUTH_TOKEN}`,
    });
  });
});
