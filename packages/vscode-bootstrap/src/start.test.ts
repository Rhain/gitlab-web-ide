import { ErrorType } from '@gitlab/web-ide-types';
import webIdeExtensionMeta from '@gitlab/vscode-extension-web-ide/vscode.package.json';
import { postMessage, createCommands } from '@gitlab/vscode-mediator-commands';
import {
  createClientOnlyConfig,
  createFakePartial,
  createRemoteHostConfig,
} from '@gitlab/utils-test';
import vscodeVersion from '@gitlab/vscode-build/vscode_version.json';
import type { AuthProvider } from '@gitlab/gitlab-api-client';
import { useMockAMDEnvironment } from '../test-utils/amd';
import { startClientOnly, startRemote } from './start';
import type { ISecretStorageProvider, WorkbenchModule } from './vscode';
import { createDefaultSecretStorageProvider } from './vscode';
import { getAuthProvider } from './utils/getAuthProvider';

const CLIENT_ONLY_TEST_CONFIG = createClientOnlyConfig();
const REMOTE_TEST_CONFIG = createRemoteHostConfig();
const TEST_MEDIATOR_COMMANDS = [{ id: 'command', handler: jest.fn() }];
const TEST_AUTH_PROVIDER: AuthProvider = {
  getHeaders: () => Promise.resolve({}),
};
const TEST_SECRET_STORAGE_PROVIDER = createFakePartial<ISecretStorageProvider>({});

jest.mock('@gitlab/vscode-mediator-commands');
jest.mock('./utils/getAuthProvider');
jest.mock('./vscode/secrets/factory');

describe('vscode-bootstrap start', () => {
  const amd = useMockAMDEnvironment();

  const workbenchDisposeSpy = jest.fn();
  const workbenchModule = {
    create: jest.fn(),
    events: {
      onWillShutdown: jest.fn(),
    },
    URI: {
      // TODO: Chad "This is weird"
      parse: (x: string) => `URI.parse-${x}`,
      from: (x: Record<string, string>) => `URI.from-${x.scheme}-${x.path}-${x.authority}}`,
    },
    logger: {
      log: jest.fn(),
    },
  };
  const bufferModule = {};

  const triggerOnWillShutdown = () => {
    const [handler] = workbenchModule.events.onWillShutdown.mock.calls[0];

    handler();
  };

  beforeAll(() => {
    amd.shim();
  });

  beforeEach(() => {
    // We have to do spy setup like mockReturnValue in a `beforeEach`. Otherwise
    // Jest will clear it out after the first run.
    workbenchModule.create.mockReturnValue({ dispose: workbenchDisposeSpy });

    amd.define<WorkbenchModule>('vs/workbench/workbench.web.main', () => workbenchModule);
    amd.define('vs/base/common/buffer', () => bufferModule);

    jest.mocked(createCommands).mockReturnValue(Promise.resolve(TEST_MEDIATOR_COMMANDS));
    jest.mocked(getAuthProvider).mockReturnValue(TEST_AUTH_PROVIDER);
    jest.mocked(createDefaultSecretStorageProvider).mockReturnValue(TEST_SECRET_STORAGE_PROVIDER);
  });

  afterEach(() => {
    amd.cleanup();
  });

  describe('startClientOnly', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let subject: any;

    const callStartClientOnly = async () => {
      await startClientOnly(CLIENT_ONLY_TEST_CONFIG);

      expect(workbenchModule.create).toHaveBeenCalledTimes(1);

      [[, subject]] = workbenchModule.create.mock.calls;
    };

    describe('with basic configuration', () => {
      beforeEach(async () => {
        await callStartClientOnly();
      });

      it('creates workbench on body', () => {
        expect(workbenchModule.create).toHaveBeenCalledWith(document.body, subject);
      });

      it('creates workbench with enabledExtensions', () => {
        const { publisher, name } = webIdeExtensionMeta;

        expect(subject).toMatchObject({
          enabledExtensions: [`${publisher}.${name}`],
        });
      });

      it('creates a workbench with a custom windowIndicator', () => {
        expect(subject).toMatchObject({
          windowIndicator: {
            label: '$(gitlab-tanuki) GitLab',
            command: 'gitlab-web-ide.open-remote-window',
          },
        });
      });

      it('creates workbench with trusted domains', () => {
        expect(subject).toMatchObject({
          additionalTrustedDomains: [
            'gitlab.com',
            'about.gitlab.com',
            'docs.gitlab.com',
            'gitlab.com',
            'foo.bar',
          ],
        });
      });

      it('creates workbench with secret storage provider', () => {
        expect(createDefaultSecretStorageProvider).toHaveBeenCalledWith(TEST_AUTH_PROVIDER);

        expect(subject.secretStorageProvider).toBe(TEST_SECRET_STORAGE_PROVIDER);
      });

      it('has default layout', () => {
        expect(subject).toMatchObject({
          defaultLayout: {
            force: true,
            editors: [],
          },
        });
      });

      it('sets commit to same as vscode_version.json', () => {
        expect(subject.productConfiguration.commit).toBe(vscodeVersion.commit);
      });

      it('sets the default configuration for font family', () => {
        expect(subject.configurationDefaults['editor.fontFamily']).toBe('monospace');
      });

      it('sets mediator commands', () => {
        expect(subject.commands).toBe(TEST_MEDIATOR_COMMANDS);
      });

      it('creates auth provider with config', () => {
        expect(getAuthProvider).toHaveBeenCalledTimes(1);
        expect(getAuthProvider).toHaveBeenCalledWith(CLIENT_ONLY_TEST_CONFIG);
      });

      it('creates commands with auth provider', () => {
        expect(createCommands).toHaveBeenCalledTimes(1);
        expect(createCommands).toHaveBeenCalledWith(
          expect.objectContaining(CLIENT_ONLY_TEST_CONFIG),
          expect.anything(),
          TEST_AUTH_PROVIDER,
        );
      });
    });

    describe('with configured fonts', () => {
      beforeEach(async () => {
        await startClientOnly({
          ...CLIENT_ONLY_TEST_CONFIG,
          editorFont: {
            fallbackFontFamily: 'monospace',
            fontFaces: [
              {
                family: 'GitLab Mono',
                src: [
                  {
                    url: '/GitLabMono.woff2',
                    format: 'woff2',
                  },
                ],
              },
              {
                family: 'GitLab Mono',
                style: 'italic',
                src: [
                  {
                    url: '/GitLabMonoItalic.woff2',
                    format: 'woff2',
                  },
                ],
              },
            ],
          },
        });

        expect(workbenchModule.create).toHaveBeenCalledTimes(1);

        [[, subject]] = workbenchModule.create.mock.calls;
      });

      it('sets the default configuration for font family', () => {
        expect(subject.configurationDefaults['editor.fontFamily']).toBe("'GitLab Mono', monospace");
      });
    });

    describe('without authProvider', () => {
      beforeEach(async () => {
        jest.mocked(getAuthProvider).mockReturnValue(undefined);

        await callStartClientOnly();
      });

      it('should not set secretStorageProvider', () => {
        expect(createDefaultSecretStorageProvider).not.toHaveBeenCalled();

        expect(subject.secretStorageProvider).toBeUndefined();
      });
    });
  });

  describe('startRemote', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let subject: any;

    beforeEach(() => {
      startRemote(REMOTE_TEST_CONFIG);

      expect(workbenchModule.create).toHaveBeenCalledTimes(1);

      [[, subject]] = workbenchModule.create.mock.calls;
    });

    it('creates workbench on body', () => {
      expect(workbenchModule.create).toHaveBeenCalledWith(document.body, subject);
    });

    it('creates workbench without extra extensions', () => {
      expect(subject).not.toHaveProperty('enabledExtensions');
    });

    it('sets remote development properties', () => {
      expect(subject).toMatchObject({
        remoteAuthority: REMOTE_TEST_CONFIG.remoteAuthority,
        connectionToken: REMOTE_TEST_CONFIG.connectionToken,
      });
    });

    it('adds custom workspace provider', async () => {
      expect(subject.workspaceProvider.workspace.folderUri).toBe(
        `URI.from-vscode-remote-${REMOTE_TEST_CONFIG.hostPath}-${REMOTE_TEST_CONFIG.remoteAuthority}}`,
      );
      await expect(subject.workspaceProvider.open()).resolves.toBe(false);
    });

    it('handles onWillShutdown event', () => {
      expect(workbenchModule.events.onWillShutdown).toHaveBeenCalledWith(expect.any(Function));
    });

    // why: base assertion for other tests that postMessage is not called by default
    it('sends a web-ide-tracking message containing a remote-connection-start tracking event', () => {
      expect((postMessage as jest.Mock).mock.calls).toHaveLength(1);
      expect(postMessage).toHaveBeenCalledWith({
        key: 'web-ide-tracking',
        params: {
          event: {
            name: 'remote-connection-start',
          },
        },
      });
    });

    // why: base assertion for other tests that dispose is not called by default
    it('workbench dispose should not be called', () => {
      expect(workbenchDisposeSpy).not.toHaveBeenCalled();
    });
  });

  describe('when onWillShutdown event is triggered', () => {
    beforeEach(() => {
      startRemote(REMOTE_TEST_CONFIG);
    });

    it('when onWillShutdown is triggered, posts close message', () => {
      triggerOnWillShutdown();

      expect(postMessage).toHaveBeenCalledWith({
        key: 'close',
      });
    });
  });

  describe('when startRemote raises an exception', () => {
    const error = new Error();

    beforeEach(() => {
      workbenchModule.create.mockImplementationOnce(() => {
        throw error;
      });
      startRemote(REMOTE_TEST_CONFIG);
    });

    it('posts START_WORKBENCH_FAILED error message', () => {
      expect(postMessage).toHaveBeenCalledWith({
        key: 'error',
        params: { errorType: ErrorType.START_WORKBENCH_FAILED },
      });
    });

    it('logs the workbench creation error', () => {
      expect(workbenchModule.logger.log).toHaveBeenCalledWith(expect.any(Number), String(error));
    });
  });
});
