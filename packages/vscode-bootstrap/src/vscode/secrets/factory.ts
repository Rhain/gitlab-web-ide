import type { AuthProvider } from '@gitlab/gitlab-api-client';
import type { ISecretStorageProvider } from '../types';
import { InMemorySecretStorageProvider } from './InMemorySecretStorageProvider';
import { OverlaySecretStorageProvider } from './OverlaySecretStorageProvider';
import { ReadonlySecretStorageProvider } from './ReadonlySecretStorageProvider';

const EXPECTED_EXTENSIONS = ['gitlab.gitlab-web-ide', 'gitlab.gitlab-workflow'];
const AUTH_KEY = 'auth_headers';
const EXPECTED_AUTH_KEYS = new Set(
  EXPECTED_EXTENSIONS.map(extensionId => JSON.stringify({ extensionId, key: AUTH_KEY })),
);

export const createDefaultSecretStorageProvider = (
  authProvider: AuthProvider,
): ISecretStorageProvider => {
  const isAuthKey = (key: string) => EXPECTED_AUTH_KEYS.has(key);
  const getAuthValue = (): Promise<string> => authProvider.getHeaders().then(JSON.stringify);

  const readonlyProvider = new ReadonlySecretStorageProvider([[isAuthKey, getAuthValue]]);
  const writableProvider = new InMemorySecretStorageProvider();

  return new OverlaySecretStorageProvider(readonlyProvider, writableProvider);
};
