import type { AuthProvider } from '@gitlab/gitlab-api-client';
import type { ISecretStorageProvider } from '../types';
import { createDefaultSecretStorageProvider } from './factory';

const TEST_AUTH_HEADERS = {
  'SECRET-TOKEN': 'abc123',
  'X-LOREM-IPSUM': '5',
};
const TEST_CUSTOM_KEY = 'custom-key';
const TEST_CUSTOM_VALUE = 'Lorem ipsum dolar sit amit.';

describe('vscode/secrets/factory', () => {
  // note: These tests should provide sufficient coverage for OverlaySecretStorageProvider
  describe('createDefaultSecretStorageProvider', () => {
    let authProvider: AuthProvider;
    let subject: ISecretStorageProvider;

    beforeEach(async () => {
      authProvider = {
        getHeaders: () => Promise.resolve(TEST_AUTH_HEADERS),
      };
      subject = createDefaultSecretStorageProvider(authProvider);

      // note: This provides coverage for "after set on custom key"
      await subject.set(TEST_CUSTOM_KEY, TEST_CUSTOM_VALUE);
    });

    it('has in-memory type', () => {
      expect(subject.type).toBe('in-memory');
    });

    it.each`
      key                                                                               | expectation
      ${JSON.stringify({ extensionId: 'gitlab.gitlab-web-ide', key: 'auth_headers' })}  | ${JSON.stringify(TEST_AUTH_HEADERS)}
      ${JSON.stringify({ extensionId: 'gitlab.gitlab-workflow', key: 'auth_headers' })} | ${JSON.stringify(TEST_AUTH_HEADERS)}
      ${JSON.stringify({ extensionId: 'vim', key: 'auth_headers' })}                    | ${undefined}
      ${TEST_CUSTOM_KEY}                                                                | ${TEST_CUSTOM_VALUE}
      ${'dne'}                                                                          | ${undefined}
    `('get for key="$key", returns $expectation', async ({ key, expectation }) => {
      const result = await subject.get(key);

      expect(result).toBe(expectation);
    });

    describe('after set on expected auth_headers key', () => {
      beforeEach(async () => {
        await subject.set(
          JSON.stringify({ extensionId: 'gitlab.gitlab-web-ide', key: 'auth_headers' }),
          'new value',
        );
      });

      it('overwrites auth_headers value', async () => {
        const result = await subject.get(
          JSON.stringify({ extensionId: 'gitlab.gitlab-web-ide', key: 'auth_headers' }),
        );

        expect(result).toBe('new value');
      });
    });

    describe('after delete on expected auth_headers key', () => {
      beforeEach(async () => {
        await subject.delete(
          JSON.stringify({ extensionId: 'gitlab.gitlab-web-ide', key: 'auth_headers' }),
        );
      });

      it('does nothing', async () => {
        const result = await subject.get(
          JSON.stringify({ extensionId: 'gitlab.gitlab-web-ide', key: 'auth_headers' }),
        );

        expect(result).toBe(JSON.stringify(TEST_AUTH_HEADERS));
      });
    });

    describe('after delete of custom_key', () => {
      beforeEach(async () => {
        await subject.delete(TEST_CUSTOM_KEY);
      });

      it('deletes custom key', async () => {
        const result = await subject.get(TEST_CUSTOM_KEY);

        expect(result).toBeUndefined();
      });
    });
  });
});
