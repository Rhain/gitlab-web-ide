import type { VSCodeBuffer } from '@gitlab/vscode-mediator-commands';

// region: VSCode module paths -----------------------------------------

export const MODULE_WORKBENCH_MAIN = 'vs/workbench/workbench.web.main';
export const MODULE_COMMON_BUFFER = 'vs/base/common/buffer';

// region: VSCode internal types ---------------------------------------

// This should match https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/blob/main/src/vs/platform/secrets/common/secrets.ts
export interface ISecretStorageProvider {
  type: 'in-memory' | 'persisted' | 'unknown';
  get(key: string): Promise<string | undefined>;
  set(key: string, value: string): Promise<void>;
  delete(key: string): Promise<void>;
}

// LogLevel https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/blob/main/src/vs/platform/log/common/log.ts
export enum LogLevel {
  Trace,
  Debug,
  Info,
  Warning,
  Error,
  Critical,
  Off,
}

// IWorkbenchConstructionOptions from https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/blob/1076180257af86c0f540faf7f6087041bd37ef8c/src/vs/workbench/browser/web.api.ts#L127
// TODO: It would be great to figure out how we can reuse the typing from the vscode-fork
export type IWorkbenchConstructionOptions = Record<string, unknown>;

// https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/blob/fa3eb589de07ab4db0500c32519ce41940c11241/src/vs/base/common/uri.ts#L98
export type URI = unknown;

// IDisposable from https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/blob/fa3eb589de07ab4db0500c32519ce41940c11241/src/vs/base/common/lifecycle.ts#L119
export interface IDisposable {
  dispose(): void;
}

// https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/blob/03eeab97952dc1d10f846b0f3ebd404e941ddf7a/src/vs/workbench/workbench.web.main.ts#L194
export interface WorkbenchModule {
  create(el: Element, options: IWorkbenchConstructionOptions): IDisposable;
  readonly events: {
    onWillShutdown(listener: (e: unknown) => unknown): Promise<void>;
  };
  readonly logger: {
    log(level: LogLevel, message: string): void;
  };
  readonly URI: {
    parse(x: string): URI;
    from(x: Record<string, string>): unknown;
  };
}

// https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/blob/559e9beea981b47ffd76d90158ccccafef663324/src/vs/base/common/buffer.ts#L15
export interface BufferModule {
  readonly VSBuffer: {
    wrap(x: Uint8Array): VSCodeBuffer;
  };
}
