import './amd/global.d';

import type { RemoteConfig, ClientOnlyConfig, BaseConfig } from '@gitlab/web-ide-types';
import { ErrorType } from '@gitlab/web-ide-types';
import { escapeCssQuotedValue } from '@gitlab/utils-escape';
import { createCommands, postMessage } from '@gitlab/vscode-mediator-commands';
import { createBufferWrapper } from './utils/createBufferWrapper';
import { getRepoRoot } from './utils/getRepoRoot';
import type { WorkbenchModule, BufferModule } from './vscode';
import {
  MODULE_COMMON_BUFFER,
  MODULE_WORKBENCH_MAIN,
  LogLevel,
  createDefaultSecretStorageProvider,
} from './vscode';
import { getAuthProvider } from './utils/getAuthProvider';

const getDomainFromFullUrl = (urlStr: string) => {
  const url = new URL(urlStr);

  return url.host;
};

const amdModuleName = (funcName: string) => `gitlab-web-ide/${funcName}`;

const BASE_OPTIONS = {
  // implements IWorkbenchConstructionOptions https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/blob/1076180257af86c0f540faf7f6087041bd37ef8c/src/vs/workbench/browser/web.api.ts#L127
  developmentOptions: {
    // TODO: Make logLevel configurable
    logLevel: LogLevel.Info,
  },
  homeIndicator: {
    href: 'https://gitlab.com',
    icon: 'code',
    title: 'GitLab',
  },
  windowIndicator: {
    label: '$(gitlab-tanuki) GitLab',
    command: 'gitlab-web-ide.open-remote-window',
  },
  defaultLayout: {
    views: [],
    editors: [],
    force: true,
  },
  settingsSyncOptions: {
    enabled: false,
  },
  additionalTrustedDomains: ['gitlab.com', 'about.gitlab.com', 'docs.gitlab.com'],
  productConfiguration: {
    // implements Partial<IProductConfiguration> https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/blob/11b6d009a4ec1567b50ba2c0ac5235d5db8ba1e9/src/vs/base/common/product.ts#L33
    // example https://sourcegraph.com/github.com/sourcegraph/openvscode-server@3169b2e0423a56afba4fa1c824f966e7b3b9bf07/-/blob/product.json?L586
    nameShort: 'GitLab Web IDE',
    nameLong: 'GitLab Web IDE',
    applicationName: 'gitlab-web-ide',
    enableTelemetry: false,
    extensionsGallery: null,
    licenseName: 'MIT License',
    licenseUrl: 'https://gitlab.com/gitlab-org/gitlab-web-ide/-/blob/main/LICENSE',
    licenseFileName: 'LICENSE',
    twitterUrl: 'https://twitter.com/gitlab',
    documentationUrl: 'https://docs.gitlab.com/ee/user/project/web_ide/',
    sendASmile: {
      reportIssueUrl: 'https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Bug',
      requestFeatureUrl:
        'https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20Proposal%20%2D%20basic',
    },
    reportIssueUrl: 'https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Bug',
    requestFeatureUrl:
      'https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20Proposal%20%2D%20basic',
    downloadUrl: '',
    updateUrl: '',
    releaseNotesUrl: 'https://about.gitlab.com/releases/categories/releases/',
    crashReporter: {
      companyName: 'GitLab',
      productName: 'Web IDE',
    },
    keyboardShortcutsUrlMac: '',
    keyboardShortcutsUrlLinux: '',
    keyboardShortcutsUrlWin: '',
    introductoryVideosUrl: '',
    tipsAndTricksUrl: '',
    newsletterSignupUrl: '',
    reportMarketplaceIssueUrl:
      'https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Bug',
    privacyStatementUrl: '',
    showTelemetryOptOut: false,
    // 'commit' should be same as vscode_version.json
    commit: '774feda4aac0eab2f971ba4d53d078d0f0a43654',
    quality: 'stable',
    webviewContentExternalBaseUrlTemplate:
      'https://{{uuid}}.cdn.web-ide.gitlab-static.net/web-ide-vscode/{{quality}}/{{commit}}/out/vs/workbench/contrib/webview/browser/pre/',
  },
};

const startWorkbench = (
  { create, logger, events }: WorkbenchModule,
  config: BaseConfig,
  additionalOptions: Record<string, unknown> = {},
) => {
  // Flag for whether we need to actually handle "close" event
  let skipCloseHandling = false;

  try {
    const options = {
      ...BASE_OPTIONS,
      ...additionalOptions,
    };
    options.additionalTrustedDomains = options.additionalTrustedDomains.concat([
      getDomainFromFullUrl(config.baseUrl),
    ]);

    create(document.body, options);

    window.parent.addEventListener('beforeunload', () => {
      skipCloseHandling = true;
      // TODO: Figure out how to dispose here without it disposing even when user
      //       prevents beforeunload
    });

    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    events.onWillShutdown(() => {
      if (skipCloseHandling) {
        return;
      }

      postMessage({
        key: 'close',
      });
    });
  } catch (e) {
    logger.log(LogLevel.Error, String(e));
    postMessage({
      key: 'error',
      params: {
        errorType: ErrorType.START_WORKBENCH_FAILED,
      },
    });
  }
};

export const startRemote = (config: RemoteConfig) => {
  postMessage({
    key: 'web-ide-tracking',
    params: {
      event: {
        name: 'remote-connection-start',
      },
    },
  });

  define(
    amdModuleName('startRemote'),
    [MODULE_WORKBENCH_MAIN],
    (workbenchModule: WorkbenchModule) => {
      startWorkbench(workbenchModule, config, {
        remoteAuthority: config.remoteAuthority,
        connectionToken: config.connectionToken,
        workspaceProvider: {
          workspace: {
            folderUri: workbenchModule.URI.from({
              scheme: 'vscode-remote',
              path: config.hostPath,
              authority: config.remoteAuthority,
            }),
          },
          // I don't know if we want to trust a workspace
          // trusted: true,
          // I'm pretty sure this is needed to prevent VSCode from trying to open windows within the iframe
          async open() {
            return false;
          },
        },
      });
    },
  );
};

const getConfigurationDefaultFontFamily = (editorFont: BaseConfig['editorFont']): string => {
  if (!editorFont) return 'monospace';

  const uniqueFamilies = new Set(editorFont.fontFaces.map(({ family }) => family));
  const fontFaceFamilies = Array.from(
    uniqueFamilies,
    family => `'${escapeCssQuotedValue(family)}'`,
  );

  return `${fontFaceFamilies.join(', ')}, ${editorFont.fallbackFontFamily}`;
};

const getConfigurationDefaults = (config: ClientOnlyConfig) => ({
  'workbench.colorTheme': 'GitLab Dark',
  'gitlab.aiAssistedCodeSuggestions.enabled': config.codeSuggestionsEnabled,
  'editor.fontFamily': getConfigurationDefaultFontFamily(config.editorFont),
});

export const startClientOnly = (config: ClientOnlyConfig) => {
  define(
    amdModuleName('startClientOnly'),
    [MODULE_WORKBENCH_MAIN, MODULE_COMMON_BUFFER],
    async (workbenchModule: WorkbenchModule, bufferModule: BufferModule) => {
      const repoRoot = getRepoRoot(config.projectPath);
      const authProvider = getAuthProvider(config);
      const fullConfig = {
        ...config,
        repoRoot,
      };

      startWorkbench(workbenchModule, config, {
        additionalTrustedDomains: [
          ...BASE_OPTIONS.additionalTrustedDomains,
          getDomainFromFullUrl(config.gitlabUrl),
        ],
        // what: Flag the gitlab-web-ide extension as an environment extension which cannot be disabled
        // https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/13#note_1053126388
        // https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/blob/fa3eb589de07ab4db0500c32519ce41940c11241/src/vs/workbench/browser/web.api.ts#L215
        enabledExtensions: ['gitlab.gitlab-web-ide'],
        // TODO - Maybe we want this...
        welcomeBanner: undefined,
        commands: await createCommands(fullConfig, createBufferWrapper(bufferModule), authProvider),
        configurationDefaults: getConfigurationDefaults(fullConfig),
        secretStorageProvider: authProvider
          ? createDefaultSecretStorageProvider(authProvider)
          : undefined,

        // This is needed so that we don't enter multiple workspace zone :|
        workspaceProvider: {
          workspace: { folderUri: workbenchModule.URI.parse(`gitlab-web-ide:///${repoRoot}`) },
          trusted: true,
          async open() {
            return false;
          },
        },
      });

      // We already handle onbeforeunload so prevent vscode from doing this
      window.addEventListener('beforeunload', e => {
        e.stopImmediatePropagation();
      });
    },
  );
};
