import type { defineFunc } from './types';

declare global {
  const define: defineFunc;
  const require: {
    config(options: unkown): void;
  };
}
