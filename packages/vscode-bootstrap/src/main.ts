// initialized by loading SCRIPT_VSCODE_LOADER 'vscode/out/vs/loader.js'
import type { BaseConfig, ClientOnlyConfig, RemoteConfig } from '@gitlab/web-ide-types';
import { getConfigFromDOM } from './utils/getConfigFromDOM';
import { startClientOnly, startRemote } from './start';
import { insertScript } from './utils/insertScript';
import { insertMeta } from './utils/insertMeta';
import { getConfigTypeFromDOM } from './utils/getConfigTypeFromDOM';

const SCRIPT_VSCODE_LOADER = 'vscode/out/vs/loader.js';
const SCRIPT_VSCODE_WEB_PACKAGE_PATHS = 'vscode/out/vs/webPackagePaths.js';
const SCRIPT_VSCODE_WORKBENCH_NLS = 'vscode/out/vs/workbench/workbench.web.main.nls.js';
const SCRIPT_VSCODE_WORKBENCH = 'vscode/out/vs/workbench/workbench.web.main.js';

declare global {
  interface Window {
    // initialized by loading SCRIPT_VSCODE_LOADER
    trustedTypes: {
      createPolicy(...args: unknown[]): unknown;
    };

    // initialized by loading SCRIPT_VSCODE_WEB_PACKAGE_PATHS
    webPackagePaths: Record<string, string>;
  }
}

const getExtensionConfig = async (config: BaseConfig, extensionPath: string) => {
  const extensionPackageJSONUrl = `${config.baseUrl}/vscode/extensions/${extensionPath}/package.json`;
  const rawJson = await fetch(extensionPackageJSONUrl).then(x => x.text());
  const packageJSON = JSON.parse(rawJson);

  return {
    extensionPath,
    packageJSON,
  };
};

const getBuiltInExtensions = async (config: ClientOnlyConfig) =>
  Promise.all([
    getExtensionConfig(config, 'gitlab-web-ide'),
    getExtensionConfig(config, 'gitlab-language-support-vue'),
    getExtensionConfig(config, 'gitlab-vscode-extension'),
  ]);

/**
 * This makes sure that the navigator keyboard is compatible
 *
 * VSCode reads from this global sometimes and was throwing some errors... This might not be needed...
 */
const setupNavigatorKeyboard = () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  if ((navigator as any).keyboard) {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Object.assign((navigator as any).keyboard, {
      getLayoutMap: () => Promise.resolve(new Map()),
    });
  }
};

const setupAMDRequire = (config: BaseConfig) => {
  const vscodeUrl = `${config.baseUrl}/vscode`;

  // eslint-disable-next-line no-restricted-globals, array-callback-return, prefer-arrow-callback, func-names
  Object.keys(self.webPackagePaths).map(function (key) {
    // eslint-disable-next-line no-restricted-globals
    self.webPackagePaths[
      key
      // eslint-disable-next-line no-restricted-globals
    ] = `${vscodeUrl}/bundled_node_modules/${key}/${self.webPackagePaths[key]}`;
  });

  require.config({
    baseUrl: `${vscodeUrl}/out`,
    recordStats: true,
    trustedTypesPolicy: window.trustedTypes?.createPolicy('amdLoader', {
      createScriptURL(value: string) {
        if (value.startsWith(vscodeUrl)) {
          return value;
        }
        throw new Error(`Invalid script url: ${value}`);
      },
    }),
    // eslint-disable-next-line no-restricted-globals
    paths: self.webPackagePaths,
  });
};

const main = async () => {
  const configType = getConfigTypeFromDOM();
  const config = getConfigFromDOM();

  if (configType === 'client-only') {
    const extensionList = await getBuiltInExtensions(config as ClientOnlyConfig);
    insertMeta('gitlab-builtin-vscode-extensions', extensionList);
  }

  setupNavigatorKeyboard();

  await Promise.all([
    insertScript(`${config.baseUrl}/${SCRIPT_VSCODE_LOADER}`, config.nonce),
    insertScript(`${config.baseUrl}/${SCRIPT_VSCODE_WEB_PACKAGE_PATHS}`, config.nonce),
  ]);

  setupAMDRequire(config);

  await Promise.all([
    insertScript(`${config.baseUrl}/${SCRIPT_VSCODE_WORKBENCH_NLS}`, config.nonce),
    insertScript(`${config.baseUrl}/${SCRIPT_VSCODE_WORKBENCH}`, config.nonce),
  ]);

  if (configType === 'client-only') {
    startClientOnly(config as ClientOnlyConfig);
  } else if (configType === 'remote') {
    startRemote(config as RemoteConfig);
  } else {
    // This shouldn't happen.
    throw new Error(`Unexpected config type (${configType}) when trying to start VSCode.`);
  }
};

main().catch(e => {
  // TODO: Lets do something nicer...
  // eslint-disable-next-line no-console
  console.error(e);
  // eslint-disable-next-line no-alert
  alert('An unexpected error occurred! Please open the developer console for details.');
});
