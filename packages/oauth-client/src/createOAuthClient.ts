import type { OAuthConfig } from '@gitlab/web-ide-types';
import type { OAuthClient } from './OAuthClient';
import type { OAuthTokenState } from './types';
import { DefaultOAuthStateBroadcaster } from './DefaultOAuthStateBroadcaster';
import { DefaultOAuthClient } from './OAuthClient';
import { OAuthLocalStorage } from './OAuthLocalStorage';

interface CreateOAuthClientOptions {
  readonly oauthConfig: OAuthConfig;
  readonly gitlabUrl: string;
  readonly owner?: string;
}

export const createOAuthClient = ({
  oauthConfig,
  gitlabUrl,
  owner,
}: CreateOAuthClientOptions): OAuthClient => {
  const excludeKeys: (keyof OAuthTokenState)[] = [];
  if (oauthConfig.protectRefreshToken) {
    excludeKeys.push('refreshToken');
  }

  const storage = new OAuthLocalStorage({ excludeKeys });
  const broadcaster = new DefaultOAuthStateBroadcaster();

  return new DefaultOAuthClient({
    app: {
      clientId: oauthConfig.clientId,
      callbackUrl: oauthConfig.callbackUrl,
      authorizeUrl: new URL('oauth/authorize', gitlabUrl).href,
      tokenUrl: new URL('oauth/token', gitlabUrl).href,
    },
    storage,
    broadcaster,
    owner,
    tokenLifetime: oauthConfig.tokenLifetime,
  });
};
