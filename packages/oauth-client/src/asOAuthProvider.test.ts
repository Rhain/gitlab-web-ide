import { useFakeBroadcastChannel } from '@gitlab/utils-test';
import { createOAuthClient } from './createOAuthClient';
import type { OAuthTokenState } from './types';
import { asOAuthProvider } from './asOAuthProvider';

const TEST_TOKEN: OAuthTokenState = {
  accessToken: 'test-access-token',
  expiresAt: 0,
};

describe('asOAuthProvider', () => {
  useFakeBroadcastChannel();

  it('adapts OAuthClient to AuthProvider', async () => {
    const client = createOAuthClient({
      oauthConfig: {
        type: 'oauth',
        callbackUrl: 'https://example.com/oauth_callback',
        clientId: 'ABC123-456DEF',
      },
      gitlabUrl: 'https://gdk.test:3443',
    });

    jest.spyOn(client, 'getToken').mockResolvedValue(TEST_TOKEN);

    const subject = asOAuthProvider(client);

    await expect(subject.getHeaders()).resolves.toEqual({
      Authorization: `Bearer ${TEST_TOKEN.accessToken}`,
    });
  });
});
