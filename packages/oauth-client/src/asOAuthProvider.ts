import type { AuthProvider } from '@gitlab/gitlab-api-client';
import type { TokenProvider } from './types';

class ApiClientOAuthProvider implements AuthProvider {
  readonly #client: TokenProvider;

  constructor(client: TokenProvider) {
    this.#client = client;
  }

  async getHeaders(): Promise<Readonly<Record<string, string>>> {
    const token = await this.#client.getToken();

    return {
      Authorization: `Bearer ${token.accessToken}`,
    };
  }
}

export const asOAuthProvider = (client: TokenProvider) => new ApiClientOAuthProvider(client);
