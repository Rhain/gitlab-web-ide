# Developer guides

## Contributing

These guides focus on general topics around contributing to the GitLab Web IDE project:

- [Development environment setup](./development_environment_setup.md) - Start here
- [Style Guide](./style_guide.md)
- [Architecture of packages](./architecture_packages.md)
- [Developer FAQ & Troubleshooting](./faq.md)

## Contributing (special cases)

These guides focus on special edge cases that might come up when contributing to the GitLab Web IDE project:

- [Working with packages](./packages.md)
- [Working with VS Code extensions](./vscode_extensions.md)
- [Fonts](./fonts.md)
- [Instrumentation](./instrumentation.md)
- [VS Code Server](./vscode-server.md)

## Maintaining

These guides focus on workflows pertaining to maintaining the GitLab Web IDE project:

- [Maintainer Responsibility](./maintainer_responsibility.md)
- [Security Releases](./security_releases.md)
